export const sizes = [
  "8px",
  "16px",
  "24px",
  "32px",
  "48px",
  "64px",
  "128px",
  "200px",
];
