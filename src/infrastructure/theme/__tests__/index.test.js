import React from "react";
import renderer from "react-test-renderer";
import { ThemeProvider } from "styled-components/native";
import { theme } from "src/infrastructure/theme/index";

it("renders correctly across screens", () => {
  const tree = renderer
    .create(
      <>
        <ThemeProvider theme={theme}>
          <theme />
        </ThemeProvider>
      </>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
