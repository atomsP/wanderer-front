import React, { useState, createContext } from "react";

export const StepCountFinishContext = createContext();

export const StepCountFinishContextProvider = ({ children }) => {
  const [stepCountFinishLocation, setStepCountFinishLocation] = useState({
    latitude: null,
    longitude: null,
    latitudeDelta: 0.001,
    longitudeDelta: 0.001,
  });

  return (
    <StepCountFinishContext.Provider
      value={{
        stepCountFinishLocation,
        setStepCountFinishLocation,
      }}
    >
      {children}
    </StepCountFinishContext.Provider>
  );
};
