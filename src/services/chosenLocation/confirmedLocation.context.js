import React, { useState, createContext } from "react";

export const ConfirmedLocationContext = createContext();

export const ConfirmedLocationContextProvider = ({ children }) => {
  const [confirmedLocation, setConfirmedLocation] = useState({
    latitude: null,
    longitude: null,
    latitudeDelta: 0.001,
    longitudeDelta: 0.001,
  });

  return (
    <ConfirmedLocationContext.Provider
      value={{
        confirmedLocation,
        setConfirmedLocation,
      }}
    >
      {children}
    </ConfirmedLocationContext.Provider>
  );
};
