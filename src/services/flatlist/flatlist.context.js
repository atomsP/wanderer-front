import React, { useState, createContext } from "react";

export const FlatlistContext = createContext();

export const FlatlistContextProvider = ({ children }) => {
  const [refresh, setRefresh] = useState(true);

  return (
    <FlatlistContext.Provider
      value={{
        refresh,
        setRefresh,
      }}
    >
      {children}
    </FlatlistContext.Provider>
  );
};
