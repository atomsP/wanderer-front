import React, { useState, createContext } from "react";

export const StepCountStartContext = createContext();

export const StepCountStartContextProvider = ({ children }) => {
  const [stepCountStartLocation, setStepCountStartLocation] = useState({
    latitude: null,
    longitude: null,
    latitudeDelta: 0.001,
    longitudeDelta: 0.001,
  });

  return (
    <StepCountStartContext.Provider
      value={{
        stepCountStartLocation,
        setStepCountStartLocation,
      }}
    >
      {children}
    </StepCountStartContext.Provider>
  );
};
