import React, { useState, createContext } from "react";

export const MapModalContext = createContext();

export const MapModalContextProvider = ({ children }) => {
  const [modalVisible, setModalVisible] = useState(false);

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [latitude, setLatitude] = useState("");
  const [longitude, setLongitude] = useState("");
  const [photo, setPhoto] = useState("");
  const [id, setId] = useState("");

  return (
    <MapModalContext.Provider
      value={{
        id,
        setId,
        modalVisible,
        setModalVisible,
        title,
        setTitle,
        description,
        setDescription,
        latitude,
        setLatitude,
        longitude,
        setLongitude,
        photo,
        setPhoto,
      }}
    >
      {children}
    </MapModalContext.Provider>
  );
};
