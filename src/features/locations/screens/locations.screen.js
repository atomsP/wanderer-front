import React, { useState, useEffect, useContext } from "react";
import { View, FlatList, Alert, Modal, Pressable } from "react-native";
import { Searchbar } from "react-native-paper";
import DropDownPicker from "react-native-dropdown-picker";
import { Divider } from "react-native-paper";
import Spinner from "react-native-loading-spinner-overlay";
import axios from "axios";

import { AxiosContext } from "src/services/axios/AxiosContext";
import { AuthContext } from "src/services/authorization/AuthContext";
import { FlatlistContext } from "src/services/flatlist/flatlist.context";

import {
  BackButton,
  SaveButton,
  ShowLocationButton,
  AddToTourButton,
} from "components/buttons/modalButtons";
import { Spacer } from "components/spacer/spacer.component";
import {
  LocationCard,
  LocationCardCover,
  Info,
  Section,
  ExpCardContainer,
  InnerContainer,
} from "components/flatlist/info-card.styles";
import {
  Container,
  SearchContainer,
  Label,
  Paragraph,
  ButtonishComponentContainer,
} from "components/styling/styled.components";

export const LocationsScreen = ({ navigation }) => {
  const [locations, setLocations] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");
  const axiosContext = useContext(AxiosContext);
  const { refresh, setRefresh } = useContext(FlatlistContext);
  const authContext = useContext(AuthContext);

  //Drop down picker states
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [dropDownData, setDropDownData] = useState([]);

  const onChangeSearch = (query) => setSearchQuery(query);

  const filteredData = searchQuery
    ? locations.filter((x) => {
        return (
          x.title.toLowerCase().includes(searchQuery.toLowerCase()) ||
          x.description.toLowerCase().includes(searchQuery.toLowerCase())
        );
      })
    : value
    ? locations.filter((x) =>
        x.category.toLowerCase().includes(value.toLowerCase())
      )
    : locations;

  useEffect(() => {
    setLoading(true);
    axiosContext.publicAxios
      .get("/locations/")
      .then((response) => {
        setLoading(false);
        let dropData = [];
        setLocations(response.data);

        response.data.forEach((element) => {
          dropData.push({
            label: element.category,
            value: element.category,
          });
        });

        dropData = dropData.filter(
          (value, index, self) =>
            index ===
            self.findIndex((t) =>
              t.label === value.label ? t.value === value.value : null
            )
        );
        dropData.push({ label: "All", value: "" });
        setDropDownData(dropData);
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert("Blunder..", "Could not retrieve locations");
      });
  }, []);

  const SaveLocation = (id) => {
    setLoading(true);
    axios
      .post("http://193.219.91.103:5100/api/user-saved-locations/", {
        location: id,
      })
      .then(() => {
        setLoading(false);
        setRefresh(!refresh);
        Alert.alert("Success!", "Location has been saved");
      })
      .catch(function (error) {
        setLoading(false);
        Alert.alert(
          "Blunder..",
          "Perhaps location is already saved, or network error"
        );
      });
  };

  const AddToTour = (id) => {
    setLoading(true);
    axios
      .post("http://193.219.91.103:5100/api/user-saved-locations-temporary/", {
        location: id,
      })
      .then(() => {
        setLoading(false);
        setRefresh(!refresh);
        Alert.alert("Success!", "Location has been added to the custom tour");
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert(
          "Blunder..",
          "Perhaps location is already added, or network error"
        );
      });
  };

  const SavedLocationInfoCard = ({ location = {} }) => {
    const [modalVisible, setModalVisible] = useState(false);

    const {
      title,
      id,
      photo,
      address: { latitude, longitude },
      description,
    } = location;

    return (
      <>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <ExpCardContainer>
            <InnerContainer>
              <LocationCard elevation={5}>
                <LocationCardCover
                  //style={{ width: "500px", height: "500px" }}
                  resizeMode={"cover"}
                  key={id}
                  source={{ uri: photo }}
                />
                <Info>
                  <Label>{title}</Label>
                  <Section>
                    <Paragraph>{description}</Paragraph>
                  </Section>
                  <Paragraph>{`${latitude}, ${longitude}`}</Paragraph>
                </Info>
                <View>
                  <>
                    {authContext?.authState?.authenticated ? (
                      <>
                        <AddToTourButton
                          onPress={() => {
                            AddToTour(id);
                            setModalVisible(false);
                          }}
                        />
                        <Divider />
                        <ShowLocationButton
                          onPress={() => {
                            navigation.navigate("LocationMapView", {
                              location,
                            });
                            setModalVisible(false);
                          }}
                        />
                        <Divider />
                        <BackButton
                          onPress={() => {
                            setModalVisible(false);
                          }}
                        />
                        <Divider />
                        <SaveButton
                          onPress={() => {
                            SaveLocation(id);
                            setModalVisible(false);
                          }}
                        />
                      </>
                    ) : (
                      <BackButton
                        onPress={() => {
                          setModalVisible(false);
                        }}
                      />
                    )}
                  </>
                </View>
              </LocationCard>
            </InnerContainer>
          </ExpCardContainer>
        </Modal>

        <Pressable onPress={() => setModalVisible(true)}>
          <LocationCard elevation={5}>
            <LocationCardCover key={title} source={{ uri: photo }} />
            <Info>
              <Label>{title}</Label>
              <Section>
                <Paragraph>{description}</Paragraph>
              </Section>
              <Paragraph>{`${latitude}, ${longitude}`}</Paragraph>
            </Info>
          </LocationCard>
        </Pressable>
      </>
    );
  };

  return (
    <Container>
      <Spinner visible={loading} />
      <Label>Category</Label>
      <ButtonishComponentContainer>
        <DropDownPicker
          placeholder="Select by category"
          open={open}
          value={value}
          items={dropDownData}
          setOpen={setOpen}
          setValue={setValue}
          setItems={setDropDownData}
        />
      </ButtonishComponentContainer>
      <SearchContainer>
        <Searchbar
          placeholder="Search by name"
          onChangeText={onChangeSearch}
          value={searchQuery}
        />
      </SearchContainer>
      <FlatList
        data={filteredData}
        renderItem={({ item }) => {
          return (
            <Spacer position="top" size="large">
              <SavedLocationInfoCard location={item} />
            </Spacer>
          );
        }}
        keyExtractor={(item) => item.id.toString()}
        initialNumToRender={3}
        extraData={locations}
      />
    </Container>
  );
};
