import React, { useState, useEffect, useContext } from "react";
import * as ImagePicker from "react-native-image-picker";
import { TextInput } from "react-native-paper";
import DropDownPicker from "react-native-dropdown-picker";
import { View, Alert } from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import axios from "axios";

import { AxiosContext } from "src/services/axios/AxiosContext";
import { ConfirmedLocationContext } from "src/services/chosenLocation/confirmedLocation.context";

import { ImagePickerModal } from "../components/imagePickerModal";
import { ImagePickerPreview } from "../components/imagePickerPreview";

import {
  Container,
  Link,
  Paragraph,
  LinkContainer,
  PicturePreviewContainer,
  InnerPicturePreviewContainer,
  ButtonishComponentContainer,
} from "components/styling/styled.components";
import { SaveButton } from "components/buttons/modalButtons";

export const SaveNewLocationScreen = ({ navigation }) => {
  const [locations, setLocations] = useState([]);
  const [loading, setLoading] = useState(false);
  const [locationName, setLocationName] = useState("");
  const [locationDescription, setLocationDescription] = useState("");
  const [pickerResponse, setPickerResponse] = useState(null);
  const [visible, setVisible] = useState(false);
  const axiosContext = useContext(AxiosContext);

  //Drop down picker states
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [dropDownData, setDropDownData] = useState([]);

  const confirmedLocationContext = useContext(ConfirmedLocationContext);

  useEffect(() => {
    axiosContext.publicAxios
      .get("/locations/")
      .then((response) => {
        let dropData = [];
        setLocations(response.data);

        response.data.forEach((element) => {
          dropData.push({
            label: element.category,
            value: element.category,
          });
        });

        dropData = dropData.filter(
          (value, index, self) =>
            index ===
            self.findIndex((t) =>
              t.label === value.label ? t.value === value.value : null
            )
        );
        setDropDownData(dropData);
      })
      .catch((error) => {});
  }, []);

  const onImageLibraryPress = () => {
    const options = {
      selectionLimit: 1,
      mediaType: "photo",
      includeBase64: true,
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      setPickerResponse(response);
      if (response.didCancel) {
        Alert.alert("Picture is required to save new location");
      } else if (response.error) {
        Alert.alert("ImagePicker Error: ", response.error);
      }
    });
  };

  const onCameraPress = () => {
    const options = {
      mediaType: "photo",
      maxWidth: 1024,
      maxHeight: 768,
      saveToPhotos: true,
      includeBase64: true,
    };
    ImagePicker.launchCamera(options, (response) => {
      setPickerResponse(response);
      if (response.didCancel) {
        Alert.alert("Picture is required to save new location");
      } else if (response.error) {
        Alert.alert("ImagePicker Error: ", response.error);
      }
    });
  };

  const postAxios = () => {
    setLoading(true);
    axios({
      method: "post",
      url: "http://193.219.91.103:5100/api/locations/",
      data: {
        title: locationName,
        description: locationDescription,
        address: {
          latitude: confirmedLocationContext.confirmedLocation.latitude,
          longitude: confirmedLocationContext.confirmedLocation.longitude,
        },
        photo: pickerResponse.assets[0].base64,
        category: value,
      },
    })
      .then((response) => {
        setLoading(false);
        Alert.alert("Success!", "Location is saved");
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert("Blunder..", "Location is not saved");
      });
  };

  const CategoryDropdown = () => {
    return (
      <>
        <ButtonishComponentContainer>
          <DropDownPicker
            placeholder={value}
            open={open}
            value={value}
            items={dropDownData}
            setOpen={setOpen}
            setValue={setValue}
            searchPlaceholder="Search Or Add New"
            searchable={true}
            addCustomItem={true}
          />
        </ButtonishComponentContainer>
      </>
    );
  };

  const uri = pickerResponse?.assets && pickerResponse.assets[0].uri;

  return (
    <Container>
      <Spinner visible={loading} />
      <TextInput
        mode="outlined"
        label="Enter location name"
        value={locationName}
        onChangeText={(text) => setLocationName(text)}
        style={{ backgroundColor: "#FFFFFF" }}
      />
      <TextInput
        mode="outlined"
        label="Enter location description"
        value={locationDescription}
        onChangeText={(text) => setLocationDescription(text)}
        style={{ backgroundColor: "#FFFFFF" }}
      />
      <CategoryDropdown />
      <View>
        <LinkContainer
          onPress={() => {
            navigation.navigate("SetLocationMapView");
          }}
        >
          <Link>Set Location</Link>
          {confirmedLocationContext.confirmedLocation.latitude ? (
            <Paragraph>
              Confirmed Location:,{" "}
              {confirmedLocationContext.confirmedLocation.latitude},{" "}
              {confirmedLocationContext.confirmedLocation.longitude}
            </Paragraph>
          ) : null}
        </LinkContainer>
      </View>
      <PicturePreviewContainer>
        <InnerPicturePreviewContainer>
          <LinkContainer onPress={() => setVisible(true)}>
            <Link>Add picture</Link>
          </LinkContainer>
        </InnerPicturePreviewContainer>
        <View>
          <ImagePickerPreview uri={uri} />
        </View>
        <ImagePickerModal
          isVisible={visible}
          onClose={() => setVisible(false)}
          onImageLibraryPress={onImageLibraryPress}
          onCameraPress={onCameraPress}
        />
      </PicturePreviewContainer>
      {pickerResponse?.assets ? (
        <>
          <ButtonishComponentContainer>
            <SaveButton onPress={postAxios} />
          </ButtonishComponentContainer>
        </>
      ) : null}
    </Container>
  );
};
