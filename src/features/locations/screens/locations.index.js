import React, { useContext } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import { AuthContext } from "src/services/authorization/AuthContext";

import { SetLocationMapViewScreen } from "src/features/locations/components/setLocation.mapView";
import { LocationMapViewScreen } from "components/locationMapView/locationMapView";
import { LocationsScreen } from "./locations.screen";
import { SaveNewLocationScreen } from "./saveNewLocation.screen";
import { ConfirmedLocationContextProvider } from "src/services/chosenLocation/confirmedLocation.context";

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

export const LocationsIndex = () => {
  const authContext = useContext(AuthContext);

  function LiveLocationsNavigator() {
    return (
      <NavigationContainer
        independent={true}
        initialRouteName={"Locations"}
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="Locations" component={LocationsScreen} />
          <Stack.Screen
            name="LocationMapView"
            component={LocationMapViewScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  function AddNewLocationNavigator() {
    return (
      <ConfirmedLocationContextProvider>
        <NavigationContainer
          independent={true}
          initialRouteName={"Save"}
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}
          >
            <Stack.Screen name="Save" component={SaveNewLocationScreen} />
            <Stack.Screen
              name="SetLocationMapView"
              component={SetLocationMapViewScreen}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </ConfirmedLocationContextProvider>
    );
  }

  return (
    <Tab.Navigator
      initialRouteName="Locations"
      screenOptions={{
        tabBarActiveTintColor: "#E91E63",
        tabBarLabelStyle: { fontSize: 12 },
        tabBarStyle: {
          backgroundColor: "#FFFFFF",
        },
      }}
    >
      <Tab.Screen
        name="Live Locations"
        component={LiveLocationsNavigator}
        options={{ tabBarLabel: "Live Locations" }}
      />
      {authContext?.authState?.authenticated ? (
        <Tab.Screen
          name="Add New"
          component={AddNewLocationNavigator}
          options={{ tabBarLabel: "Add New" }}
        />
      ) : null}
    </Tab.Navigator>
  );
};
