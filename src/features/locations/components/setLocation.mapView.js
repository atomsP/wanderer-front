import React, { useContext, useEffect, useState } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import MapView, { Marker, UrlTile } from "react-native-maps";
import styled from "styled-components";
import Geolocation from "@react-native-community/geolocation";

import { LocationPickerModal } from "./locationPickerModal";
import { ConfirmedLocationContext } from "src/services/chosenLocation/confirmedLocation.context";

const MarkerImage = styled.Image`
  width: ${(props) => props.theme.sizes[3]};
  height: ${(props) => props.theme.sizes[3]};
`;

const { width, height } = Dimensions.get("window");

export const SetLocationMapViewScreen = ({ navigation }) => {
  const [position, setPosition] = useState({
    latitude: 10,
    longitude: 10,
    latitudeDelta: 0.001,
    longitudeDelta: 0.001,
  });
  const [chosenLocation, setChosenLocation] = useState({
    latitude: position.latitude,
    longitude: position.longitude,
    latitudeDelta: 0.001,
    longitudeDelta: 0.001,
  });
  const [visible, setVisible] = useState(false);
  const confirmedLocationContext = useContext(ConfirmedLocationContext);

  useEffect(() => {
    Geolocation.getCurrentPosition((pos) => {
      const crd = pos.coords;
      setPosition({
        latitude: crd.latitude,
        longitude: crd.longitude,
        latitudeDelta: 0.0421,
        longitudeDelta: 0.0421,
      });
      console.log(position);
    });
    // .catch((err) => {
    //   console.log(err);
    // });
  }, []);

  const onMyLocationPress = () => {
    confirmedLocationContext.setConfirmedLocation({
      latitude: position.latitude,
      longitude: position.longitude,
      latitudeDelta: 0.001,
      longitudeDelta: 0.001,
    });
    setVisible(false);
    navigation.navigate("Save");
  };

  const onChosenLocationPress = () => {
    confirmedLocationContext.setConfirmedLocation({
      latitude: chosenLocation.latitude,
      longitude: chosenLocation.longitude,
      latitudeDelta: 0.001,
      longitudeDelta: 0.001,
    });
    setVisible(false);
    navigation.navigate("Save");
  };

  return (
    <View style={styles.container}>
      <MapView
        mapType={"none"}
        style={styles.map}
        initialRegion={{
          latitude: 54.687157,
          longitude: 25.279652,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}
        showsUserLocation={true}
        showsMyLocationButton={true}
        followsUserLocation={true}
        showsCompass={true}
        scrollEnabled={true}
        zoomEnabled={true}
        pitchEnabled={true}
        onPress={(e) => {
          setChosenLocation({
            latitude: e.nativeEvent.coordinate.latitude,
            longitude: e.nativeEvent.coordinate.longitude,
            latitudeDelta: 0.001,
            longitudeDelta: 0.001,
          });
          setVisible(true);
        }}
      >
        <Marker
          title="You are here"
          coordinate={position}
          tracksViewChanges={false}
        >
          <MarkerImage source={require("assets/iconnew.png")} />
        </Marker>
        {chosenLocation.latitude !== position.latitude ? (
          <Marker title="Chosen Location" coordinate={chosenLocation} />
        ) : null}
        <UrlTile
          urlTemplate={"http://193.219.91.103:5204/tile/{z}/{x}/{y}.png"}
          zIndex={1}
        />
      </MapView>
      <LocationPickerModal
        isVisible={visible}
        onClose={() => setVisible(false)}
        onMyLocationPress={onMyLocationPress}
        onChosenLocationPress={onChosenLocationPress}
        tracksViewChanges={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    height: height,
    width: width,
    justifyContent: "flex-start",
    alignItems: "stretch",
  },
});
