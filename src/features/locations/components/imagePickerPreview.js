import React from "react";
import styled from "styled-components/native";

import { images } from "../assets";

const PortraitImage = styled.Image`
  height: 250px;
  width: 250px;
  overflow: hidden;
  bordercolor: ${(props) => props.theme.colors.bg.primary};
`;

export const ImagePickerPreview = ({ uri }) => {
  return <PortraitImage source={uri ? { uri } : images.camera} />;
};
