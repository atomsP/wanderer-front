import React from "react";
import styled from "styled-components/native";
import Modal from "react-native-modal";

import { images } from "../assets";

const Buttons = styled.View`
  background-color: ${(props) => props.theme.colors.bg.primary};
  flex-direction: row;
  border-top-right-radius: ${(props) => props.theme.space[4]};
  border-top-left-radius: ${(props) => props.theme.space[4]};
`;
const Button = styled.Pressable`
  flex: 1;
  justify-content: center;
  align-items: center;
`;
const ButtonIcon = styled.Image`
  width: ${(props) => props.theme.sizes[3]};
  height: ${(props) => props.theme.sizes[3]};
  margin: ${(props) => props.theme.space[2]};
`;
const ButtonText = styled.Text`
  fontsize: ${(props) => props.theme.fontSizes.button};
  fontweight: ${(props) => props.theme.fontWeights.medium}px;
  color: ${(props) => props.theme.colors.ui.primary};
`;

export const ImagePickerModal = ({
  isVisible,
  onClose,
  onImageLibraryPress,
  onCameraPress,
}) => {
  return (
    <Modal
      isVisible={isVisible}
      onBackButtonPress={onClose}
      onBackdropPress={onClose}
      style={{ justifyContent: "flex-end", margin: 0 }}
    >
      <Buttons>
        <Button onPress={onImageLibraryPress}>
          <ButtonIcon source={images.image} />
          <ButtonText>Library</ButtonText>
        </Button>
        <Button onPress={onCameraPress}>
          <ButtonIcon source={images.camera} />
          <ButtonText>Camera</ButtonText>
        </Button>
      </Buttons>
    </Modal>
  );
};
