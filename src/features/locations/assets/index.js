export const images = {
  image: require("./image.jpg"),
  camera: require("./camera.png"),
  myLocation: require("./my-location.png"),
  chosenLocation: require("./chosenLocation.png"),
};
