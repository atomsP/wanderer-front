import React from "react";

import { Logo } from "../components/Logo";
import { JustButton, OutlinedButton } from "components/buttons/modalButtons";

import { Container } from "../components/loginGroupStyles";

export const StartScreen = ({ navigation }) => {
  return (
    <Container>
      <Logo />
      <JustButton
        mode="contained"
        onPress={() => navigation.navigate("LoginScreen")}
      >
        Login
      </JustButton>

      <OutlinedButton
        mode="contained"
        onPress={() => navigation.navigate("RegisterScreen")}
      >
        Sign Up
      </OutlinedButton>
    </Container>
  );
};
