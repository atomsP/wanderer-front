import React, { useState, useContext } from "react";
import { TouchableOpacity, Alert } from "react-native";
import styled from "styled-components/native";
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import * as Keychain from "react-native-keychain";

import { AuthContext } from "src/services/authorization/AuthContext";

import { Logo } from "../components/Logo";
import { JustButton } from "components/buttons/modalButtons";
import { TextInput } from "../components/TextInput";
import { BackButton } from "../components/BackButton";
import { Header, Container, Row, Link } from "../components/loginGroupStyles";
import { Label, Paragraph } from "components/styling/styled.components";
import { nameValidator } from "../components/nameValidator";
import { passwordValidator } from "../components/passwordValidator";

const ContainerOnRight = styled.View`
  width: 100%;
  align-items: flex-end;
  margin-bottom: 24px;
`;

export const LoginScreen = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState({ value: "", error: "" });
  //const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });

  const authContext = useContext(AuthContext);
  //const publicAxios = useContext(AxiosContext);

  const onLogin = () => {
    //const emailError = emailValidator(email.value);
    const usernameError = nameValidator(username.value);
    const passwordError = passwordValidator(password.value);
    if (usernameError || passwordError) {
      //setEmail({ ...email, error: emailError });
      setUsername({ ...username, error: usernameError });
      setPassword({ ...password, error: passwordError });
      return;
    }
    postAxios();
  };

  const postAxios = () => {
    setLoading(true);
    axios
      .post("http://193.219.91.103:5100/api/login/", {
        username: username.value,
        password: password.value,
      })
      .then((response) => {
        setLoading(false);
        const { message } = response.data;
        const accessToken = message;
        authContext.setAuthState({
          accessToken,
          authenticated: true,
        });
        Keychain.setGenericPassword(
          "token",
          JSON.stringify({
            accessToken,
          })
        );
        navigation.reset({
          index: 0,
          routes: [{ name: "AuthorizedScreen" }],
        });
      })
      .catch(function (error) {
        setLoading(false);
        Alert.alert("Awry..", "Bad credentials or network error");
      });
  };

  return (
    <Container>
      <Spinner visible={loading} />
      <BackButton goBack={navigation.goBack} />
      <Logo />
      <Header>Welcome back</Header>
      <Paragraph>Login into app for full functionality</Paragraph>
      {/* <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: "" })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      /> */}
      <TextInput
        label="Username"
        returnKeyType="next"
        value={username.value}
        onChangeText={(text) => setUsername({ value: text, error: "" })}
        error={!!username.error}
        errorText={username.error}
      />
      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({ value: text, error: "" })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <ContainerOnRight>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("UnauthorizedScreen");
          }}
        >
          <Link>Continue as a guest</Link>
        </TouchableOpacity>
      </ContainerOnRight>
      <ContainerOnRight>
        <TouchableOpacity
          onPress={() => navigation.navigate("ResetPasswordScreen")}
        >
          <Link>Forgot your password?</Link>
        </TouchableOpacity>
      </ContainerOnRight>
      <JustButton mode="contained" onPress={onLogin}>
        Login
      </JustButton>
      <Row>
        <Label>Don’t have an account?</Label>
        <TouchableOpacity onPress={() => navigation.replace("RegisterScreen")}>
          <Link> Sign up</Link>
        </TouchableOpacity>
      </Row>
    </Container>
  );
};
