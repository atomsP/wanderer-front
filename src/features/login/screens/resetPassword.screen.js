import React, { useState } from "react";

import { BackButton } from "../components/BackButton";
import { Logo } from "../components/Logo";
import { TextInput } from "../components/TextInput";
import { JustButton } from "components/buttons/modalButtons";
import { emailValidator } from "../components/emailValidator";
import { Header, Container } from "../components/loginGroupStyles";

export const ResetPasswordScreen = ({ navigation }) => {
  const [email, setEmail] = useState({ value: "", error: "" });

  const sendResetPasswordEmail = () => {
    const emailError = emailValidator(email.value);
    if (emailError) {
      setEmail({ ...email, error: emailError });
      return;
    }
    navigation.navigate("LoginScreen");
  };

  return (
    <Container>
      <BackButton goBack={navigation.goBack} />
      <Logo />
      <Header>Restore Password</Header>
      <TextInput
        label="E-mail address"
        returnKeyType="done"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: "" })}
        error={email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
        description="You will receive email with password reset link."
      />
      <JustButton mode="contained" onPress={sendResetPasswordEmail}>
        Send Instructions
      </JustButton>
    </Container>
  );
};
