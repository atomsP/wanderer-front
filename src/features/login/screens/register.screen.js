import React, { useState, useContext } from "react";
import { TouchableOpacity, Alert } from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import axios from "axios";

import { Logo } from "../components/Logo";
import { JustButton } from "components/buttons/modalButtons";
import { TextInput } from "../components/TextInput";
import { BackButton } from "../components/BackButton";
import { Header, Link, Container, Row } from "../components/loginGroupStyles";
import { Label } from "components/styling/styled.components";
import { passwordValidator } from "../components/passwordValidator";
import { nameValidator } from "../components/nameValidator";

//import { AxiosContext } from "../../../services/axios/AxiosContext";

export const RegisterScreen = ({ navigation }) => {
  //const { publicAxios } = useContext(AxiosContext);
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState("");
  const [first_name, setFirst_name] = useState("");
  const [last_name, setLast_name] = useState("");
  //const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState("");

  const onRegister = () => {
    const usernameError = nameValidator(username.value);
    const first_nameError = nameValidator(first_name.value);
    const last_nameError = nameValidator(last_name.value);
    //const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    if (usernameError || passwordError || first_nameError || last_nameError) {
      setUsername({ ...username, error: usernameError });
      setFirst_name({ ...first_name, error: first_nameError });
      setLast_name({ ...last_name, error: last_nameError });
      //setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return;
    }
    postAxios();
  };

  // const postAxios = async () => {
  //   try {
  //     const response = await publicAxios.post("/register/", {
  //       username: username.value,
  //       password: password.value,
  //       last_name: last_name.value,
  //       first_name: first_name.value,
  //     });

  //     Alert.alert("SignUp Successful", "Login using your credentials");
  //     navigation.replace("LoginScreen");
  //   } catch (error) {
  //     Alert.alert("SignUp Failed", "Username already exists");
  //   }
  // };
  const postAxios = () => {
    setLoading(true);
    axios
      .post("http://193.219.91.103:5100/api/register/", {
        username: username.value,
        password: password.value,
        last_name: last_name.value,
        first_name: first_name.value,
      })
      .then((response) => {
        Alert.alert("SignUp Successful", "Login using your credentials");
        navigation.replace("LoginScreen");
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert("Awry..", "Existing user or network error");
      });
  };

  return (
    <Container>
      <Spinner visible={loading} />
      <BackButton goBack={navigation.goBack} />
      <Logo />
      <Header>Create Account</Header>
      <TextInput
        label="Username"
        returnKeyType="next"
        value={username.value}
        onChangeText={(text) => setUsername({ value: text, error: "" })}
        error={!!username.error}
        errorText={username.error}
      />
      <TextInput
        label="First Name"
        returnKeyType="next"
        value={first_name.value}
        onChangeText={(text) => setFirst_name({ value: text, error: "" })}
        error={!!first_name.error}
        errorText={first_name.error}
      />
      <TextInput
        label="Last Name"
        returnKeyType="next"
        value={last_name.value}
        onChangeText={(text) => setLast_name({ value: text, error: "" })}
        error={!!last_name.error}
        errorText={last_name.error}
      />
      {/* <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: "" })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      /> */}
      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({ value: text, error: "" })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <JustButton mode="contained" onPress={onRegister}>
        Sign Up
      </JustButton>
      <Row>
        <Label>Already have an account? </Label>
        <TouchableOpacity onPress={() => navigation.replace("LoginScreen")}>
          <Link>Login</Link>
        </TouchableOpacity>
      </Row>
    </Container>
  );
};
