import { passwordValidator } from "../components/passwordValidator";

it("password validator should return error if input is left empty", () => {
  expect(passwordValidator("")).toBe("Password can't be empty.");
  expect(passwordValidator("abcde")).toBe("");
});
