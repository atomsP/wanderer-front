import { nameValidator } from "../components/nameValidator";

it("name validator should return error if input is left empty", () => {
  expect(nameValidator("")).toBe("Name can't be empty.");
  expect(nameValidator("a")).toBe("");
});
