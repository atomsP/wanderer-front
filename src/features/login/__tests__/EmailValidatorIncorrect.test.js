import { emailValidator } from "../components/emailValidator";

it("email input should include @", () => {
  expect(emailValidator("test.email.com")).toBe(
    "We need a valid email address."
  );
  expect(emailValidator("test@email.com")).toBe("");
});
