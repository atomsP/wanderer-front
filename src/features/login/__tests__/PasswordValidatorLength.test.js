import { passwordValidator } from "../components/passwordValidator";

it("password validator should return error input length is < 5", () => {
  expect(passwordValidator("abcd")).toBe(
    "Password must be at least 5 characters long."
  );
  expect(passwordValidator("abcde")).toBe("");
});
