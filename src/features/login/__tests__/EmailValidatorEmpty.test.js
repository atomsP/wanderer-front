import { emailValidator } from "../components/emailValidator";

it("email input should not be empty", () => {
  expect(emailValidator("")).toBe("Email can't be empty.");
  expect(emailValidator("test@email.com")).toBe("");
});
