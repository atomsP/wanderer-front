import styled from "styled-components/native";

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: ${(props) => props.theme.space[3]};
  background-color: ${(props) => props.theme.colors.bg.primary};
`;

export const Header = styled.Text`
  font-size: ${(props) => props.theme.fontSizes.title};
  padding: ${(props) => props.theme.space[3]};
  color: #262626;
`;

export const Paragraph = styled.Text`
  font-size: ${(props) => props.theme.fontSizes.caption};
  color: #262626;
`;

export const Link = styled.Text`
  font-weight: bold;
  color: #262626;
`;

export const Row = styled.View`
  flex-direction: row;
  margin-top: 4px;
`;
