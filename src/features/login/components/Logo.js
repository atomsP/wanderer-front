import React from "react";
import styled from "styled-components/native";

const LogoImage = styled.Image`
  width: 110px;
  height: 110px;
`;

export const Logo = () => {
  return <LogoImage source={require("assets/iconnew.png")} />;
};
