import React from "react";
import styled from "styled-components/native";
import { TextInput as Input } from "react-native-paper";

const TextInputContainer = styled.View`
  width: 100%;
  margin-vertical: ${(props) => props.theme.space[3]};
`;
const Description = styled.Text`
  fontsize: ${(props) => props.theme.sizes[1]};
  color: ${(props) => props.theme.colors.ui.success};
  paddingtop: ${(props) => props.theme.space[2]};
`;
const Error = styled.Text`
  fontsize: ${(props) => props.theme.sizes[1]};
  color: ${(props) => props.theme.colors.ui.error};
  paddingtop: ${(props) => props.theme.space[2]};
`;
const TextInputField = styled(Input)`
  background-color: ${(props) => props.theme.colors.bg.primary};
`;

export const TextInput = ({ errorText, description, ...props }) => {
  return (
    <TextInputContainer>
      <TextInputField underlineColor="transparent" mode="outlined" {...props} />
      {description && !errorText ? (
        <Description>{description}</Description>
      ) : null}
      {errorText ? <Error>{errorText}</Error> : null}
    </TextInputContainer>
  );
};
