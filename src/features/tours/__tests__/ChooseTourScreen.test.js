import React from "react";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });

import { shallow } from "enzyme";
import { ChooseScreen } from "../screens/choose.tour";

describe("ModifyStockTour screen integration test", () => {
  it("renders the screen with all components", () => {
    const wrapper = shallow(<ChooseScreen />);
    expect(wrapper.find("JustButton"));
    expect(wrapper.find("Container"));
  });
});
