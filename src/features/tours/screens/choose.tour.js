import React, { useState } from "react";
import { View } from "react-native";
import Dialog from "react-native-dialog";

import { JustButton } from "components/buttons/modalButtons";
import { Container } from "components/styling/styled.components";

export const ChooseScreen = ({ navigation }) => {
  const [visible, setVisible] = useState(false);

  const showDialog = () => {
    setVisible(true);
  };

  const onCreateNew = () => {
    navigation.replace("CreateNewTourScreen");
  };

  const modifySaved = () => {
    navigation.replace("ModifySavedTourScreen");
  };

  const modifyStock = () => {
    navigation.replace("ModifyStockTourScreen");
  };

  return (
    <Container>
      <JustButton mode="contained" onPress={onCreateNew}>
        Create New
      </JustButton>
      <JustButton mode="contained" onPress={showDialog}>
        Modify Existing
      </JustButton>
      <View>
        <Dialog.Container visible={visible}>
          <Dialog.Title>Select</Dialog.Title>
          <Dialog.Description>
            Do you want to modify saved tours or stock tours?
          </Dialog.Description>
          <Dialog.Button label="Saved" onPress={modifySaved} />
          <Dialog.Button label="Stock" onPress={modifyStock} />
        </Dialog.Container>
      </View>
    </Container>
  );
};
