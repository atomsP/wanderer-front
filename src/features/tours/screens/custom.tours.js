import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import { ChooseScreen } from "./choose.tour";
import { ModifySavedTourScreen } from "./modify.saved.tour";
import { ModifyStockTourScreen } from "./modify.stock.tour";
import { CreateNewTourScreen } from "./create.tour";
import { LocationMapViewScreen } from "components/locationMapView/locationMapView";

const Stack = createStackNavigator();

export const CustomTours = () => {
  return (
    <>
      <NavigationContainer independent={true}>
        <Stack.Navigator
          initialRouteName={"Choose"}
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="Choose" component={ChooseScreen} />
          <Stack.Screen
            name="CreateNewTourScreen"
            component={CreateNewTourScreen}
          />
          <Stack.Screen
            name="ModifySavedTourScreen"
            component={ModifySavedTourScreen}
          />
          <Stack.Screen
            name="ModifyStockTourScreen"
            component={ModifyStockTourScreen}
          />
          <Stack.Screen
            name="LocationMapViewScreen"
            component={LocationMapViewScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};
