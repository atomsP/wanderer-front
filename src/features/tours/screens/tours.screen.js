import React, { useContext } from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

import { AuthContext } from "src/services/authorization/AuthContext";
import { StepCountStartContextProvider } from "src/services/stepCountStart/stepCountStart";
import { StepCountFinishContextProvider } from "src/services/stepCountFinish/stepCountFinish";

import { CustomTours } from "./custom.tours";
import { RecommendedTours } from "./recommended.tours";
import { StockTours } from "./stock.tours";
import { StepCountTours } from "./StepCountTours";
import { MapViewScreen } from "components/tourMapView/mapView";
import { StepCountMapViewScreen } from "./stepCountMapView.screen";
import { LocationMapViewScreen } from "components/locationMapView/locationMapView";

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

export const ToursScreen = () => {
  const authContext = useContext(AuthContext);

  function StockNavigator() {
    return (
      <NavigationContainer independent={true}>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="Stock" component={StockTours} />
          <Stack.Screen name="MapView" component={MapViewScreen} />
          <Stack.Screen
            name="LocationMapView"
            component={LocationMapViewScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  function StepCountNavigator() {
    return (
      <StepCountStartContextProvider>
        <StepCountFinishContextProvider>
          <NavigationContainer independent={true}>
            <Stack.Navigator
              screenOptions={{
                headerShown: false,
              }}
            >
              <Stack.Screen name="Step Count" component={StepCountTours} />
              <Stack.Screen name="MapView" component={StepCountMapViewScreen} />
            </Stack.Navigator>
          </NavigationContainer>
        </StepCountFinishContextProvider>
      </StepCountStartContextProvider>
    );
  }

  return (
    <>
      {authContext?.authState?.authenticated ? (
        <Tab.Navigator
          initialRouteName="Step Count"
          screenOptions={{
            tabBarActiveTintColor: "#e91e63",
            tabBarLabelStyle: { fontSize: 12 },
            tabBarStyle: {
              backgroundColor: "#FFFFFF",
            },
          }}
        >
          <Tab.Screen
            name="Step Count"
            component={StepCountNavigator}
            options={{ tabBarLabel: "Step Count" }}
          />
          <Tab.Screen
            name="Recommendations"
            component={RecommendedTours}
            options={{ tabBarLabel: "Hints" }}
          />
          <Tab.Screen
            name="Custom"
            component={CustomTours}
            options={{ tabBarLabel: "Custom" }}
          />
          <Tab.Screen
            name="Stock"
            component={StockNavigator}
            options={{ tabBarLabel: "Stock" }}
          />
        </Tab.Navigator>
      ) : (
        <Tab.Navigator
          initialRouteName="Stock"
          screenOptions={{
            tabBarActiveTintColor: "#e91e63",
            tabBarLabelStyle: { fontSize: 12 },
            tabBarStyle: {
              backgroundColor: "#FFFFFF",
            },
          }}
        >
          <Tab.Screen
            name="Stock"
            component={StockNavigator}
            options={{ tabBarLabel: "Stock" }}
          />
        </Tab.Navigator>
      )}
    </>
  );
};
