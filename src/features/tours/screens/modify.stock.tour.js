import React from "react";
import { TouchableOpacity, Image } from "react-native";
import {
  Container,
  Link,
  ButtonishComponentContainer,
} from "components/styling/styled.components";

export const ModifyStockTourScreen = ({ navigation }) => {
  return (
    <Container>
      <ButtonishComponentContainer>
        <TouchableOpacity
          onPress={() => {
            navigation.replace("Choose");
          }}
        >
          <Link>Go Back</Link>
        </TouchableOpacity>
      </ButtonishComponentContainer>
      <Image
        style={{ width: 300, height: 200, backgroundColor: "transparent" }}
        source={require("assets/work_progress.jpg")}
      />
    </Container>
  );
};
