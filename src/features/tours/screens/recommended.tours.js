import React from "react";
import { Text, Image } from "react-native";
import styled from "styled-components";

const Container = styled.View`
  flex: 1;
  background-color: white;
`;

export const RecommendedTours = () => {
  return (
    <Container>
      <Image
        style={{ width: 300, height: 200, backgroundColor: "transparent" }}
        source={require("assets/work_progress.jpg")}
      />
      <Text style={{ color: "#262626" }}>
        Please try Custom and Stock tours!
      </Text>
    </Container>
  );
};
