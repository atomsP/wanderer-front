import React, { useContext, useEffect, useState } from "react";
import {
  View,
  TouchableOpacity,
  Modal,
  Alert,
  Pressable,
  FlatList,
} from "react-native";
import { TextInput } from "react-native-paper";
import axios from "axios";
import styled from "styled-components/native";
import Spinner from "react-native-loading-spinner-overlay";
import { Divider } from "react-native-paper";

import { FlatlistContext } from "src/services/flatlist/flatlist.context";
import { AxiosContext } from "src/services/axios/AxiosContext";

import { Spacer } from "components/spacer/spacer.component";
import {
  TourCard,
  LocationCard,
  LocationCardCover,
  Info,
  Section,
  ExpCardContainer,
  InnerContainer,
} from "components/flatlist/info-card.styles";
import {
  Container,
  Label,
  Paragraph,
  ButtonishComponentContainer,
  SearchContainer,
} from "components/styling/styled.components";
import {
  SelectStartingPoint,
  BackButton,
  DeleteButton,
  ShowLocationButton,
  RemoveLocationsButton,
  BuildTourButton,
} from "components/buttons/modalButtons";

const TopContainer = styled.View`
  flex-direction: row;
`;
const ArrowContainer = styled.View`
  flex: 1;
  justify-content: center;
`;
const ButtonContainer = styled.View`
  flex: 8;
`;
const BackImage = styled.Image`
  width: 30px;
  height: 20px;
  background-color: transparent;
`;

export const CreateNewTourScreen = ({ navigation }) => {
  const [locations, setLocations] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [tourName, setTourName] = React.useState("");
  const [changedLocations, setChangedLocations] = React.useState([]);
  let tmplocations = [];
  const [tourDescription, setTourDescription] = React.useState("");
  const axiosContext = useContext(AxiosContext);
  const { refresh, setRefresh } = useContext(FlatlistContext);

  useEffect(() => {
    setLoading(true);
    axiosContext.publicAxios
      .get("/user-saved-locations-temporary/")
      .then((response) => {
        setLoading(false);
        setLocations(response.data);
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert("Blunder..", "Could not retrieve custom locations");
      });
  }, [refresh, axiosContext.publicAxios]);

  function BuildTour() {
    return (
      <>
        {!tourName.trim() || !tourDescription.trim() || locationIDs.length === 0
          ? Alert.alert(
              "Please Enter Both Tour Name And Tour Description",
              "Also make sure you have selected the starting location"
            )
          : postAxios()}
      </>
    );
  }

  const locationIDs = changedLocations.map((a) => a.id);

  const postAxios = () => {
    setLoading(true);
    console.log("Before post", changedLocations);
    console.log("Location ids ", locationIDs);
    axios
      .post("http://193.219.91.103:5100/api/tours/", {
        title: tourName,
        description: tourDescription,
        city: 1,
        locations: locationIDs,
      })
      .then(() => {
        setLoading(false);
        Alert.alert(
          "Tour has been created",
          "You can find it in stock tours section now"
        );
        setTourName("");
        setTourDescription("");
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert("Blunder..", "Tour creation failed");
      });
  };

  const DeleteAllLocations = () => {
    setLoading(true);
    axios
      .delete(
        "http://193.219.91.103:5100/api/delete-user-saved-locations-temporary/"
      )
      .then(() => {
        setLoading(false);
        setRefresh(!refresh);
        setChangedLocations([]);
        Alert.alert("Success!", "All Locations have been deleted!");
      })
      .catch(function (error) {
        setLoading(false);
        Alert.alert("Blunder..", "Network Flop");
      });
  };

  const SelectAsStartingPoint = (locationId) => {
    tmplocations = locations.slice();
    let index = tmplocations
      .map(function (e) {
        return e.id;
      })
      .indexOf(locationId);
    swap(index);
  };

  function swap(index) {
    const temp = tmplocations[index];
    tmplocations[index] = tmplocations[0];
    tmplocations[0] = temp;
    setChangedLocations(tmplocations);
    Alert.alert(`${tmplocations[0].title} is now starting point!`);
  }

  useEffect(() => {}, [changedLocations]);

  const SavedLocationInfoCard = ({ location = {} }) => {
    const [modalVisible, setModalVisible] = useState(false);

    const {
      title,
      id,
      photo,
      address: { latitude, longitude },
      description,
    } = location;

    const DeleteLocation = (id) => {
      setLoading(true);
      axios
        .delete(
          `http://193.219.91.103:5100/api/user-saved-locations-temporary/${id}/`
        )
        .then(() => {
          setLoading(false);
          setRefresh(!refresh);
          Alert.alert("Success!", "Obliterated!");
        })
        .catch((error) => {
          setLoading(false);
          Alert.alert("Blunder..", "Could not remove, network error");
        });
    };

    return (
      <>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <ExpCardContainer>
            <InnerContainer>
              <LocationCard elevation={5}>
                <LocationCardCover key={id} source={{ uri: photo }} />
                <Info>
                  <Label>{title}</Label>
                  <Section>
                    <Paragraph>{description}</Paragraph>
                  </Section>
                </Info>
                <View>
                  <SelectStartingPoint
                    onPress={() => {
                      SelectAsStartingPoint(id);
                      setModalVisible(!modalVisible);
                    }}
                  />
                  <Divider />
                  <ShowLocationButton
                    onPress={() => {
                      navigation.navigate("LocationMapViewScreen", {
                        location,
                      });
                      setModalVisible(false);
                    }}
                  />
                  <Divider />
                  <DeleteButton
                    onPress={() => {
                      DeleteLocation(id);
                      setModalVisible(false);
                    }}
                  />
                  <Divider />
                  <BackButton
                    onPress={() => {
                      setModalVisible(false);
                    }}
                  />
                </View>
              </LocationCard>
            </InnerContainer>
          </ExpCardContainer>
        </Modal>

        <Pressable onPress={() => setModalVisible(true)}>
          <TourCard elevation={2}>
            <Info>
              <Label>{title}</Label>

              <Section>
                <Paragraph>{description}</Paragraph>
              </Section>
              <Section>
                <Paragraph>{`${latitude}, ${longitude}`}</Paragraph>
              </Section>
            </Info>
          </TourCard>
        </Pressable>
      </>
    );
  };

  return (
    <>
      <Container>
        <Spinner visible={loading} />
        <TopContainer>
          <ArrowContainer>
            <TouchableOpacity
              onPress={() => {
                navigation.replace("Choose");
              }}
            >
              <BackImage source={require("assets/arrow_back.png")} />
            </TouchableOpacity>
          </ArrowContainer>
          {locations.length !== 0 ? (
            <ButtonContainer>
              <TouchableOpacity
                onPress={() => {
                  DeleteAllLocations();
                }}
              >
                <RemoveLocationsButton />
              </TouchableOpacity>
            </ButtonContainer>
          ) : null}
        </TopContainer>
        <ButtonishComponentContainer>
          <TextInput
            mode="outlined"
            label="Enter tour name"
            value={tourName}
            onChangeText={(text) => setTourName(text)}
            style={{ backgroundColor: "#FFFFFF" }}
          />
          <TextInput
            mode="outlined"
            label="Enter tour description"
            value={tourDescription}
            onChangeText={(text) => setTourDescription(text)}
            style={{ backgroundColor: "#FFFFFF" }}
          />
          <ButtonishComponentContainer>
            <BuildTourButton
              onPress={() => {
                BuildTour();
              }}
            />
          </ButtonishComponentContainer>
        </ButtonishComponentContainer>

        {typeof changedLocations !== "undefined" && changedLocations.length ? (
          <SearchContainer>
            <Label>
              Starting location is set as {changedLocations[0].title}{" "}
            </Label>
          </SearchContainer>
        ) : null}
        {locations.length !== 0 ? (
          <FlatList
            data={locations}
            extraData={changedLocations}
            renderItem={({ item }) => {
              return (
                <Spacer position="top" size="large">
                  <SavedLocationInfoCard location={item} />
                </Spacer>
              );
            }}
            keyExtractor={(item) => item.id.toString()}
          />
        ) : (
          <SearchContainer>
            <Label>No locations saved</Label>
          </SearchContainer>
        )}
      </Container>
    </>
  );
};
