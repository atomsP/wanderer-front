import React, { useContext, useEffect, useState } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { Button } from "react-native-paper";
import MapView, { Marker, UrlTile } from "react-native-maps";
import styled from "styled-components";
import Geolocation from "@react-native-community/geolocation";

import { StepCountStartContext } from "src/services/stepCountStart/stepCountStart";
import { StepCountFinishContext } from "src/services/stepCountFinish/stepCountFinish";
import { ButtonishComponentContainer } from "components/styling/styled.components";

const MarkerImage = styled.Image`
  width: 48px;
  height: 48px;
`;
const SetButtonOuterContainer = styled.View`
  flex: 0.3;
  flex-direction: row-reverse;
`;
const SetButtonInnerContainer = styled.View`
  flex: 1;
  align-items: flex-end;
  margin-right: 16px;
`;

const { width, height } = Dimensions.get("window");

export const StepCountMapViewScreen = ({ navigation }) => {
  const [position, setPosition] = useState({
    latitude: 10,
    longitude: 10,
    latitudeDelta: 0.001,
    longitudeDelta: 0.001,
  });

  const stepCountStartContext = useContext(StepCountStartContext);
  const stepCountFinishContext = useContext(StepCountFinishContext);

  const start = () => {
    return stepCountStartContext.stepCountStartLocation.latitude &&
      stepCountFinishContext.stepCountFinishLocation.latitude
      ? {
          latitude: stepCountStartContext.stepCountStartLocation.latitude,
          longitude: stepCountStartContext.stepCountStartLocation.longitude,
          latitudeDelta: 0.001,
          longitudeDelta: 0.001,
        }
      : {
          latitude: 54.6884,
          longitude: 25.2741,
          latitudeDelta: 0.001,
          longitudeDelta: 0.001,
        };
  };

  const finish = () => {
    return stepCountStartContext.stepCountStartLocation.latitude &&
      stepCountFinishContext.stepCountFinishLocation.latitude
      ? {
          latitude: stepCountFinishContext.stepCountFinishLocation.latitude,
          longitude: stepCountFinishContext.stepCountFinishLocation.longitude,
          latitudeDelta: 0.001,
          longitudeDelta: 0.001,
        }
      : {
          latitude: 54.6751019,
          longitude: 25.2735587,
          latitudeDelta: 0.001,
          longitudeDelta: 0.001,
        };
  };

  useEffect(() => {
    Geolocation.getCurrentPosition((pos) => {
      const crd = pos.coords;
      setPosition({
        latitude: crd.latitude,
        longitude: crd.longitude,
        latitudeDelta: 0.0421,
        longitudeDelta: 0.0421,
      });
      console.log(position);
    });
    // .catch((err) => {
    //   console.log(err);
    // });
  }, []);

  const OverlayComponent = () => {
    return (
      <ButtonishComponentContainer>
        <Button
          mode="contained"
          onPress={() => {
            navigation.navigate("Step Count");
          }}
        >
          Set!
        </Button>
      </ButtonishComponentContainer>
    );
  };

  return (
    <View style={styles.container}>
      <MapView
        mapType={"none"}
        style={styles.map}
        initialRegion={{
          latitude: 54.687157,
          longitude: 25.279652,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}
        showsUserLocation={true}
        showsMyLocationButton={true}
        followsUserLocation={true}
        showsCompass={true}
        scrollEnabled={true}
        zoomEnabled={true}
        pitchEnabled={true}
      >
        <Marker
          title="You are here"
          coordinate={position}
          tracksViewChanges={false}
        >
          <MarkerImage source={require("assets/iconnew.png")} />
        </Marker>
        <Marker
          draggable
          title="Start"
          tracksViewChanges={false}
          coordinate={start()}
          onDragEnd={(e) =>
            stepCountStartContext.setStepCountStartLocation(
              e.nativeEvent.coordinate
            )
          }
        />
        <Marker
          draggable
          title="Finish"
          tracksViewChanges={false}
          coordinate={finish()}
          onDragEnd={(e) =>
            stepCountFinishContext.setStepCountFinishLocation(
              e.nativeEvent.coordinate
            )
          }
        />
        <UrlTile
          urlTemplate={"http://193.219.91.103:5204/tile/{z}/{x}/{y}.png"}
          zIndex={1}
        />
      </MapView>
      {stepCountStartContext.stepCountStartLocation.latitude &&
      stepCountFinishContext.stepCountFinishLocation.latitude ? (
        <SetButtonOuterContainer>
          <SetButtonInnerContainer>
            <OverlayComponent
              style={{
                position: "absolute",
              }}
            />
          </SetButtonInnerContainer>
        </SetButtonOuterContainer>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    height: height,
    width: width,
    justifyContent: "flex-start",
    alignItems: "stretch",
  },
});
