import React, { useState, useEffect, useContext } from "react";
import {
  View,
  FlatList,
  Alert,
  Modal,
  Pressable,
  TouchableOpacity,
} from "react-native";
import { Searchbar } from "react-native-paper";
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import DropDownPicker from "react-native-dropdown-picker";

import { AuthContext } from "src/services/authorization/AuthContext";
import { AxiosContext } from "src/services/axios/AxiosContext";
import { FlatlistContext } from "src/services/flatlist/flatlist.context";

import { Spacer } from "components/spacer/spacer.component";
import { Divider } from "react-native-paper";
import {
  TourCard,
  LocationCard,
  LocationCardCover,
  Info,
  Section,
  ExpCardContainer,
  InnerContainer,
} from "components/flatlist/info-card.styles";
import {
  Container,
  SearchContainer,
  Label,
  Paragraph,
  ButtonishComponentContainer,
  Link,
} from "components/styling/styled.components";
import {
  SaveTourButton,
  BackButton,
  ShowLocationButton,
  ShowTourButton,
} from "components/buttons/modalButtons";

export const StockTours = ({ navigation }) => {
  const [stockTours, setStockTours] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");

  const authContext = useContext(AuthContext);
  const axiosContext = useContext(AxiosContext);
  const { refresh, setRefresh } = useContext(FlatlistContext);

  //Drop down picker states
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [dropDownData, setDropDownData] = useState([]);

  const onChangeSearch = (query) => setSearchQuery(query);

  const filteredData = searchQuery
    ? stockTours.filter(
        (x) =>
          x.description.toLowerCase().includes(searchQuery.toLowerCase()) ||
          x.title.toLowerCase().includes(searchQuery.toLowerCase())
      )
    : stockTours && value
    ? stockTours.filter((x) =>
        x.categories_list.filter((cat) => {
          cat.toLowerCase().includes(value.toLowerCase());
        })
      )
    : stockTours;

  useEffect(() => {
    setLoading(true);
    axiosContext.publicAxios
      .get("/tours/")
      .then((response) => {
        let dropData = [];
        setLoading(false);
        setStockTours(response.data);

        response.data.forEach((element) => {
          element.categories_list.forEach((category) => {
            dropData.push({
              label: category,
              value: category,
            });
          });
        });

        dropData = dropData.filter(
          (value, index, self) =>
            index ===
            self.findIndex(
              (t) => t.label === value.label && t.value === value.value
            )
        );
        dropData.push({ label: "All", value: "" });
        setDropDownData(dropData);
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert("Blunder..", "Could not retrieve tours");
      });
  }, [refresh, axiosContext.publicAxios]);

  const SaveTour = (id) => {
    setLoading(true);
    axios
      .post("http://193.219.91.103:5100/api/user-saved-tours/", {
        tour: id,
      })
      .then(() => {
        setLoading(false);
        setRefresh(!refresh);
        Alert.alert("Success!", "Tour has been saved");
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert(
          "Blunder..",
          "Perhaps tour is already saved, or network error"
        );
      });
  };

  const SavedTourInfoCard = ({ tour = {} }) => {
    const [tourModalVisible, setTourModalVisible] = useState(false);
    const [locationModalVisible, setLocationModalVisible] = useState(false);
    const [location, setLocation] = useState([]);

    const { id, author, categories_list, title, description, locations } = tour;

    function onlyUnique(value, index, self) {
      return self.indexOf(value) === index;
    }

    const uniqueCategories = categories_list.filter(onlyUnique);

    return (
      <>
        <Modal
          animationType="slide"
          transparent={true}
          visible={tourModalVisible}
          onRequestClose={() => {
            setTourModalVisible(!tourModalVisible);
          }}
        >
          <Modal
            animationType="slide"
            transparent={true}
            visible={locationModalVisible}
            onRequestClose={() => {
              setLocationModalVisible(!locationModalVisible);
            }}
          >
            <ExpCardContainer>
              <InnerContainer>
                <LocationCard elevation={5} key={id}>
                  <LocationCardCover
                    key={id}
                    source={{ uri: location.photo }}
                  />
                  <Info>
                    <Label>{location.title}</Label>
                    <Section>
                      <Paragraph>{location.description}</Paragraph>
                    </Section>
                    <Paragraph>{`${JSON.stringify(
                      location.address
                    )}`}</Paragraph>
                  </Info>
                  <View>
                    <>
                      <ShowLocationButton
                        onPress={() => {
                          navigation.navigate("LocationMapView", {
                            location,
                          });
                        }}
                      />
                      <Divider />
                      <BackButton
                        onPress={() => {
                          setLocationModalVisible(false);
                        }}
                      />
                    </>
                  </View>
                </LocationCard>
              </InnerContainer>
            </ExpCardContainer>
          </Modal>

          <ExpCardContainer>
            <InnerContainer>
              <LocationCard elevation={5}>
                <Info>
                  <Label>{title}</Label>
                  <Section>
                    <Paragraph>
                      {description} {"\n"}Author: {author} {"\n"}
                    </Paragraph>
                  </Section>
                  <Section>
                    <View>
                      <Paragraph>Categories:</Paragraph>
                      {uniqueCategories.map((category) => {
                        return (
                          <View>
                            <Paragraph>{category}</Paragraph>
                          </View>
                        );
                      })}
                    </View>
                  </Section>
                  <Section>
                    <View>
                      <Paragraph>{"\n"}Locations:</Paragraph>
                      {locations.map((location) => {
                        return (
                          <View>
                            <TouchableOpacity
                              onPress={() => {
                                setLocation(location);
                                setLocationModalVisible(true);
                              }}
                            >
                              <Link>{location.title}</Link>
                            </TouchableOpacity>
                          </View>
                        );
                      })}
                    </View>
                  </Section>
                </Info>
                <View>
                  {authContext?.authState?.authenticated ? (
                    <SaveTourButton
                      onPress={() => {
                        SaveTour(id);
                        setTourModalVisible(false);
                      }}
                    />
                  ) : null}
                  <Divider />
                  <ShowTourButton
                    onPress={() => {
                      navigation.navigate("MapView", {
                        tour,
                      });
                      setTourModalVisible(false);
                    }}
                  />
                  <Divider />
                  <BackButton
                    onPress={() => {
                      setTourModalVisible(false);
                    }}
                  />
                </View>
              </LocationCard>
            </InnerContainer>
          </ExpCardContainer>
        </Modal>

        <Pressable onPress={() => setTourModalVisible(true)}>
          <TourCard elevation={2}>
            <Info>
              <Label>{title}</Label>
              <Section>
                <Paragraph>{description}</Paragraph>
              </Section>
            </Info>
          </TourCard>
        </Pressable>
      </>
    );
  };

  return (
    <Container>
      <Spinner visible={loading} />
      <Label>Category</Label>
      <ButtonishComponentContainer>
        <DropDownPicker
          placeholder="Select by category"
          open={open}
          value={value}
          items={dropDownData}
          setOpen={setOpen}
          setValue={setValue}
          setItems={setDropDownData}
        />
      </ButtonishComponentContainer>
      <SearchContainer>
        <Searchbar
          placeholder="Search"
          onChangeText={onChangeSearch}
          value={searchQuery}
        />
      </SearchContainer>
      <FlatList
        data={filteredData}
        extraData={stockTours}
        renderItem={({ item }) => {
          return (
            <Spacer position="top" size="large">
              <SavedTourInfoCard tour={item} />
            </Spacer>
          );
        }}
        keyExtractor={(item, id) => {
          return id.toString();
        }}
      />
    </Container>
  );
};
