import React, { useState, useEffect, useContext } from "react";
import { TouchableOpacity } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";

import { AxiosContext } from "src/services/axios/AxiosContext";
import { StepCountStartContext } from "src/services/stepCountStart/stepCountStart";
import { StepCountFinishContext } from "src/services/stepCountFinish/stepCountFinish";

import {
  Container,
  ButtonishComponentContainer,
  Link,
  Paragraph,
} from "components/styling/styled.components";
import { BuildWalkButton } from "components/buttons/modalButtons";

export const StepCountTours = ({ navigation }) => {
  const [locations, setLocations] = useState([]);
  const [loading, setLoading] = useState(false);

  const stepCountStartContext = useContext(StepCountStartContext);
  const stepCountFinishContext = useContext(StepCountFinishContext);

  //Steps drop down picker states
  const [stepsOpen, setStepsOpen] = useState(false);
  const [stepsValue, setStepsValue] = useState(null);
  const [steps, setSteps] = useState([
    { label: "1000", value: "1000" },
    { label: "2000", value: "2000" },
    { label: "3000", value: "3000" },
    { label: "4000", value: "4000" },
    { label: "5000", value: "5000" },
    { label: "6000", value: "6000" },
    { label: "7000", value: "7000" },
    { label: "8000", value: "8000" },
    { label: "9000", value: "9000" },
    { label: "10000", value: "10000" },
    { label: "12000", value: "12000" },
    { label: "15000", value: "15000" },
  ]);

  //Category drop down picker states
  const [categoryOpen, setCategoryOpen] = useState(false);
  const [categoryValue, setCategoryValue] = useState([]);
  const [category, setCategory] = useState([]);

  const axiosContext = useContext(AxiosContext);

  useEffect(() => {
    setLoading(true);
    axiosContext.publicAxios
      .get("/locations/")
      .then((response) => {
        setLoading(false);
        let dropData = [];
        setLocations(response.data);

        response.data.forEach((element) => {
          dropData.push({
            label: element.category,
            value: element.category,
          });
        });

        dropData = dropData.filter(
          (value, index, self) =>
            index ===
            self.findIndex((t) =>
              t.label === value.label ? t.value === value.value : null
            )
        );
        setCategory(dropData);
      })
      .catch((error) => {
        setLoading(false);
      });
  }, []);

  const StepDropdown = () => {
    return (
      <ButtonishComponentContainer>
        <DropDownPicker
          placeholder={"Select amount of steps"}
          open={stepsOpen}
          value={stepsValue}
          items={steps}
          setOpen={setStepsOpen}
          setValue={setStepsValue}
        />
      </ButtonishComponentContainer>
    );
  };

  const CategoryDropdown = () => {
    return (
      <ButtonishComponentContainer>
        <DropDownPicker
          placeholder={"Select categories"}
          zIndex={3000}
          zIndexInverse={1000}
          multiple={true}
          open={categoryOpen}
          value={categoryValue}
          items={category}
          setOpen={setCategoryOpen}
          setValue={setCategoryValue}
        />
      </ButtonishComponentContainer>
    );
  };

  return (
    <Container>
      <StepDropdown />
      <CategoryDropdown />
      <ButtonishComponentContainer>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("MapView");
          }}
        >
          <Link>Choose Starting and Finishing Locations</Link>
          <Paragraph>
            Long press on both Start and Finish markers to adjust position
          </Paragraph>
          {stepCountStartContext.stepCountStartLocation.latitude &&
          stepCountFinishContext.stepCountFinishLocation.latitude ? (
            <>
              <Paragraph>
                {"\n"} Start:{" "}
                {stepCountStartContext.stepCountStartLocation.latitude},{" "}
                {stepCountStartContext.stepCountStartLocation.longitude}
              </Paragraph>
              <Paragraph>
                Finish:{" "}
                {stepCountFinishContext.stepCountFinishLocation.latitude},{" "}
                {stepCountFinishContext.stepCountFinishLocation.longitude}
              </Paragraph>
            </>
          ) : null}
        </TouchableOpacity>
      </ButtonishComponentContainer>
      <ButtonishComponentContainer>
        <BuildWalkButton
          onPress={() => {
            console.log("building");
          }}
        />
      </ButtonishComponentContainer>
    </Container>
  );
};
