import React from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

import { SavedLocationsScreen } from "./saved.locations.screen";
import { SavedToursScreen } from "./saved.tours.screen";
import { MapViewScreen } from "components/tourMapView/mapView";
import { LocationMapViewScreen } from "components/locationMapView/locationMapView";

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

export const SavedScreen = () => {
  function SavedToursNavigator() {
    return (
      <NavigationContainer independent={true}>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="Stock" component={SavedToursScreen} />
          <Stack.Screen name="MapView" component={MapViewScreen} />
          <Stack.Screen
            name="LocationMapView"
            component={LocationMapViewScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
  function SavedLocationsNavigator() {
    return (
      <NavigationContainer independent={true}>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="Stock" component={SavedLocationsScreen} />
          <Stack.Screen
            name="LocationMapView"
            component={LocationMapViewScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  return (
    <Tab.Navigator
      initialRouteName="Locations"
      screenOptions={{
        tabBarActiveTintColor: "#e91e63",
        tabBarLabelStyle: { fontSize: 12 },
        tabBarStyle: {
          backgroundColor: "#FFFFFF",
        },
      }}
    >
      <Tab.Screen
        name="Locations"
        component={SavedLocationsNavigator}
        options={{ tabBarLabel: "Locations" }}
      />
      <Tab.Screen
        name="Tours"
        component={SavedToursNavigator}
        options={{ tabBarLabel: "Tours" }}
      />
    </Tab.Navigator>
  );
};
