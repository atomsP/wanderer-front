import React, { useState, useEffect, useContext } from "react";
import { View, FlatList, Alert, Modal, Pressable } from "react-native";
import { Searchbar } from "react-native-paper";
import { Divider } from "react-native-paper";
import DropDownPicker from "react-native-dropdown-picker";
import Spinner from "react-native-loading-spinner-overlay";
import axios from "axios";

import { AxiosContext } from "src/services/axios/AxiosContext";
import { FlatlistContext } from "src/services/flatlist/flatlist.context";

import { Spacer } from "components/spacer/spacer.component";
import {
  LocationCard,
  LocationCardCover,
  Info,
  Section,
  ExpCardContainer,
  InnerContainer,
} from "components/flatlist/info-card.styles";
import {
  Container,
  SearchContainer,
  Label,
  Paragraph,
  ButtonishComponentContainer,
} from "components/styling/styled.components";
import {
  BackButton,
  AddToTourButton,
  ShowLocationButton,
  DeleteButton,
} from "components/buttons/modalButtons";

export const SavedLocationsScreen = ({ navigation }) => {
  const [savedLocations, setSavedLocations] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");

  const axiosContext = useContext(AxiosContext);
  const { refresh, setRefresh } = useContext(FlatlistContext);

  //Drop down picker states
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [dropDownData, setDropDownData] = useState([]);

  const onChangeSearch = (query) => setSearchQuery(query);

  const filteredData = searchQuery
    ? savedLocations.filter((x) => {
        return (
          x.title.toLowerCase().includes(searchQuery.toLowerCase()) ||
          x.description.toLowerCase().includes(searchQuery.toLowerCase())
        );
      })
    : savedLocations && value
    ? savedLocations.filter((x) =>
        x.category.toLowerCase().includes(value.toLowerCase())
      )
    : savedLocations;

  useEffect(() => {
    setLoading(true);
    axiosContext.publicAxios
      .get("/user-saved-locations")
      .then((response) => {
        setLoading(false);
        let dropData = [];
        setSavedLocations(response.data);

        response.data.forEach((element) => {
          dropData.push({
            label: element.category,
            value: element.category,
          });
        });

        dropData = dropData.filter(
          (value, index, self) =>
            index ===
            self.findIndex(
              (t) => t.label === value.label && t.value === value.value
            )
        );
        dropData.push({ label: "All", value: "" });
        setDropDownData(dropData);
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert("Blunder..", "Could not retrieve locations");
      });
  }, [refresh, axiosContext.publicAxios]);

  const DeleteLocation = (id) => {
    setLoading(true);
    axios
      .delete(`http://193.219.91.103:5100/api/user-saved-locations/${id}/`)
      .then((response) => {
        setLoading(false);
        setRefresh(!refresh);
        Alert.alert("Success!", "Obliterated");
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert("Blunder..", "Network fiasco");
      });
  };

  const AddToTour = (id) => {
    setLoading(true);
    axios
      .post("http://193.219.91.103:5100/api/user-saved-locations-temporary/", {
        location: id,
      })
      .then((response) => {
        setLoading(false);
        setRefresh(!refresh);
        Alert.alert("Success!", "Location has been added to the custom tour");
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert(
          "Blunder..",
          "Perhaps location is already added, or network error"
        );
      });
  };

  const SavedLocationInfoCard = ({ location = {} }) => {
    const [modalVisible, setModalVisible] = useState(false);

    const {
      title,
      id,
      photo,
      address: { latitude, longitude },
      description,
    } = location;

    return (
      <>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <ExpCardContainer>
            <InnerContainer>
              <LocationCard elevation={5}>
                <LocationCardCover key={id} source={{ uri: photo }} />
                <Info>
                  <Label>{title}</Label>
                  <Section>
                    <Paragraph>{description}</Paragraph>
                  </Section>
                  <Paragraph>{`${latitude}, ${longitude}`}</Paragraph>
                </Info>
                <View>
                  <>
                    <AddToTourButton
                      onPress={() => {
                        AddToTour(id);
                        setModalVisible(false);
                      }}
                    />
                    <Divider />
                    <ShowLocationButton
                      onPress={() => {
                        navigation.navigate("LocationMapView", {
                          location,
                        });
                        setModalVisible(false);
                      }}
                    />
                    <Divider />
                    <BackButton
                      onPress={() => {
                        setModalVisible(false);
                      }}
                    />
                    <Divider />
                    <DeleteButton
                      onPress={() => {
                        DeleteLocation(id);
                        setModalVisible(false);
                      }}
                    />
                  </>
                </View>
              </LocationCard>
            </InnerContainer>
          </ExpCardContainer>
        </Modal>

        <Pressable onPress={() => setModalVisible(true)}>
          <LocationCard elevation={5}>
            <LocationCardCover key={title} source={{ uri: photo }} />
            <Info>
              <Label>{title}</Label>
              <Section>
                <Paragraph>{description}</Paragraph>
              </Section>
              <Paragraph>{`${latitude}, ${longitude}`}</Paragraph>
            </Info>
          </LocationCard>
        </Pressable>
      </>
    );
  };

  return (
    <Container>
      <Spinner visible={loading} />
      <Label>Category</Label>
      <ButtonishComponentContainer>
        <DropDownPicker
          placeholder="Select by category"
          open={open}
          value={value}
          items={dropDownData}
          setOpen={setOpen}
          setValue={setValue}
          setItems={setDropDownData}
        />
      </ButtonishComponentContainer>
      <SearchContainer>
        <Searchbar
          placeholder="Search"
          onChangeText={onChangeSearch}
          value={searchQuery}
        />
      </SearchContainer>
      <FlatList
        data={filteredData}
        extraData={savedLocations}
        renderItem={({ item }) => {
          return (
            <Spacer position="top" size="large">
              <SavedLocationInfoCard location={item} />
            </Spacer>
          );
        }}
        keyExtractor={(item, id) => {
          return id.toString();
        }}
      />
    </Container>
  );
};
