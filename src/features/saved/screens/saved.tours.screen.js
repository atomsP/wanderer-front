import React, { useState, useEffect, useContext } from "react";
import {
  View,
  FlatList,
  Alert,
  Modal,
  Pressable,
  TouchableOpacity,
} from "react-native";
import { Searchbar } from "react-native-paper";
import { Divider } from "react-native-paper";
import DropDownPicker from "react-native-dropdown-picker";
import Spinner from "react-native-loading-spinner-overlay";
import axios from "axios";

import { AxiosContext } from "src/services/axios/AxiosContext";
import { FlatlistContext } from "src/services/flatlist/flatlist.context";

import { Spacer } from "components/spacer/spacer.component";
import {
  TourCard,
  LocationCard,
  LocationCardCover,
  Info,
  Section,
  ExpCardContainer,
  InnerContainer,
} from "components/flatlist/info-card.styles";
import {
  Link,
  Container,
  SearchContainer,
  Label,
  Paragraph,
  ButtonishComponentContainer,
} from "components/styling/styled.components";
import {
  DeleteTourButton,
  BackButton,
  ShowTourButton,
} from "components/buttons/modalButtons";

export const SavedToursScreen = ({ navigation }) => {
  const [savedTours, setSavedTours] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");

  const axiosContext = useContext(AxiosContext);
  const { refresh, setRefresh } = useContext(FlatlistContext);

  //Drop down picker states
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [dropDownData, setDropDownData] = useState([]);

  const onChangeSearch = (query) => setSearchQuery(query);

  const filteredData = searchQuery
    ? savedTours.filter((x) => {
        return (
          x.title.toLowerCase().includes(searchQuery.toLowerCase()) ||
          x.description.toLowerCase().includes(searchQuery.toLowerCase())
        );
      })
    : savedTours && value
    ? savedTours.filter((x) =>
        x.categories_list.filter((cat) => {
          cat.toLowerCase().includes(value.toLowerCase());
        })
      )
    : savedTours;

  useEffect(() => {
    setLoading(true);
    axiosContext.publicAxios
      .get("/user-saved-tours/")
      .then((response) => {
        setLoading(false);
        let dropData = [];

        setSavedTours(response.data);

        response.data.forEach((element) => {
          element.categories_list.forEach((category) => {
            dropData.push({
              label: category,
              value: category,
            });
          });
        });

        dropData = dropData.filter(
          (value, index, self) =>
            index ===
            self.findIndex(
              (t) => t.label === value.label && t.value === value.value
            )
        );
        dropData.push({ label: "All", value: "" });
        setDropDownData(dropData);
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert("Blunder..", "Could not retrieve locations");
      });
  }, [refresh, axiosContext.publicAxios]);

  const DeleteTour = (id) => {
    setLoading(true);
    axios
      .delete(`http://193.219.91.103:5100/api/user-saved-tours/${id}/`)
      .then(() => {
        setLoading(false);
        setRefresh(!refresh);
        Alert.alert("Success!", "Abolished");
      })
      .catch(function (error) {
        setLoading(false);
        Alert.alert("Blunder..", "Network flop");
      });
  };

  const renderItem = ({ item }) => {
    return (
      <Spacer position="top" size="large">
        <SavedTourInfoCard tour={item} />
      </Spacer>
    );
  };

  const SavedTourInfoCard = ({ tour = {} }) => {
    const [tourModalVisible, setTourModalVisible] = useState(false);
    const [locationModalVisible, setLocationModalVisible] = useState(false);
    const [locationModalData, setLocationModalData] = useState([]);

    const { id, author, categories_list, title, description, locations } = tour;

    function onlyUnique(value, index, self) {
      return self.indexOf(value) === index;
    }

    const uniqueCategories = categories_list.filter(onlyUnique);

    return (
      <>
        <Modal
          animationType="slide"
          transparent={true}
          visible={tourModalVisible}
          onRequestClose={() => {
            setTourModalVisible(!tourModalVisible);
          }}
        >
          <Modal
            animationType="slide"
            transparent={true}
            visible={locationModalVisible}
            onRequestClose={() => {
              setLocationModalVisible(!locationModalVisible);
            }}
          >
            <ExpCardContainer>
              <InnerContainer>
                <LocationCard elevation={5}>
                  <LocationCardCover
                    key={locationModalData.id}
                    source={{ uri: locationModalData.photo }}
                  />
                  <Info>
                    <Label>{locationModalData.title}</Label>
                    <Section>
                      <Paragraph>{locationModalData.description}</Paragraph>
                    </Section>
                    <Paragraph>{`${JSON.stringify(
                      locationModalData.address
                    )}`}</Paragraph>
                  </Info>
                  <View>
                    <>
                      <Divider />
                      <BackButton
                        onPress={() => {
                          setLocationModalVisible(false);
                        }}
                      />
                    </>
                  </View>
                </LocationCard>
              </InnerContainer>
            </ExpCardContainer>
          </Modal>

          <ExpCardContainer>
            <InnerContainer>
              <LocationCard elevation={5} key={id}>
                <Info>
                  <Label>{title}</Label>
                  <Section>
                    <Paragraph>
                      {description} {"\n"}Author: {author} {"\n"}
                    </Paragraph>
                  </Section>
                  <Section>
                    <View>
                      <Paragraph>Categories:</Paragraph>
                      {uniqueCategories.map((category) => {
                        return (
                          <View>
                            <Paragraph>{category}</Paragraph>
                          </View>
                        );
                      })}
                    </View>
                  </Section>
                  <Section>
                    <View>
                      <Paragraph>{"\n"}Locations:</Paragraph>
                      {locations.map((location) => {
                        return (
                          <View>
                            <TouchableOpacity
                              onPress={() => {
                                setLocationModalData(location);
                                console.log(location.address.latitude);
                                setLocationModalVisible(true);
                              }}
                            >
                              <Link>{location.title}</Link>
                            </TouchableOpacity>
                          </View>
                        );
                      })}
                    </View>
                  </Section>
                </Info>
                <View>
                  <DeleteTourButton
                    onPress={() => {
                      DeleteTour(id);
                      setTourModalVisible(false);
                    }}
                  />
                  <Divider />
                  <ShowTourButton
                    onPress={() => {
                      navigation.navigate("MapView", {
                        tour,
                      });
                      setTourModalVisible(!tourModalVisible);
                    }}
                  />
                  <Divider />
                  <BackButton
                    onPress={() => {
                      setTourModalVisible(false);
                    }}
                  />
                </View>
              </LocationCard>
            </InnerContainer>
          </ExpCardContainer>
        </Modal>

        <Pressable onPress={() => setTourModalVisible(true)}>
          <TourCard elevation={2}>
            <Info>
              <Label>{title}</Label>
              <Section>
                <Paragraph>{description}</Paragraph>
              </Section>
            </Info>
          </TourCard>
        </Pressable>
      </>
    );
  };

  return (
    <Container>
      <Spinner visible={loading} />
      <Label>Category</Label>
      <ButtonishComponentContainer>
        <DropDownPicker
          placeholder="Select by category"
          open={open}
          value={value}
          items={dropDownData}
          setOpen={setOpen}
          setValue={setValue}
          setItems={setDropDownData}
        />
      </ButtonishComponentContainer>
      <SearchContainer>
        <Searchbar
          placeholder="Search"
          onChangeText={onChangeSearch}
          value={searchQuery}
        />
      </SearchContainer>
      <FlatList
        data={filteredData}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        initialNumToRender={10}
        extraData={savedTours}
      />
    </Container>
  );
};
