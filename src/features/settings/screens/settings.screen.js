import React, { useState, useContext, useEffect } from "react";
import * as Keychain from "react-native-keychain";
import styled from "styled-components/native";
import Spinner from "react-native-loading-spinner-overlay";

import { AuthContext } from "src/services/authorization/AuthContext";
import { AxiosContext } from "src/services/axios/AxiosContext";

import { JustButton } from "components/buttons/modalButtons";
import { Container, Label } from "components/styling/styled.components";
import { Alert } from "react-native";

const SupportInfo = styled.View`
  padding: ${(props) => props.theme.space[3]};
  background-color: ${(props) => props.theme.colors.bg.primary};
`;

export const SettingsScreen = ({ navigation }) => {
  const [userData, setUserData] = useState([]);
  const [loading, setLoading] = useState(null);

  const axiosContext = useContext(AxiosContext);
  const authContext = useContext(AuthContext);

  useEffect(() => {
    setLoading(true);
    axiosContext.publicAxios
      .get("/me/")
      .then((response) => {
        setLoading(false);
        setUserData(response.data);
      })
      .catch(function (error) {
        setLoading(false);
        Alert.alert("Blunder..", "Could not retrieve user details");
      });
  }, [axiosContext.publicAxios]);

  const onLogoutPressed = async () => {
    await Keychain.resetGenericPassword();
    authContext.setAuthState({
      accessToken: null,
      refreshToken: null,
      authenticated: false,
    });
    navigation.reset({
      index: 0,
      routes: [{ name: "StartScreen" }],
    });
  };

  return (
    <>
      <SupportInfo>
        <Label>
          Contact us by sending an email to {"\n"}help.wanderer@gmail.com
        </Label>
      </SupportInfo>
      {authContext?.authState?.authenticated ? (
        <Container>
          <Spinner visible={loading} />
          <Label>Username: {userData.username}</Label>
          <JustButton mode="contained" onPress={onLogoutPressed}>
            Logout
          </JustButton>
        </Container>
      ) : (
        <Container>
          <Spinner visible={loading} />
          <JustButton
            mode="contained"
            onPress={() => {
              navigation.reset({
                index: 0,
                routes: [{ name: "StartScreen" }],
              });
            }}
          >
            Create Acc/login
          </JustButton>
        </Container>
      )}
    </>
  );
};
