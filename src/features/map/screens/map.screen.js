import React, { useContext, useEffect, useState } from "react";
import { Alert, Modal, View, StyleSheet, Dimensions } from "react-native";
import MapView, { Marker, UrlTile } from "react-native-maps";
import styled from "styled-components";
import Geolocation from "@react-native-community/geolocation";
import { Divider } from "react-native-paper";
import Spinner from "react-native-loading-spinner-overlay";
import DropDownPicker from "react-native-dropdown-picker";
import axios from "axios";

import { AuthContext } from "src/services/authorization/AuthContext";
import { AxiosContext } from "src/services/axios/AxiosContext";
import { FlatlistContext } from "src/services/flatlist/flatlist.context";
import { MapModalContext } from "src/services/mapModal/mapModal.context";

import {
  LocationCard,
  LocationCardCover,
  Info,
  Section,
  ExpCardContainer,
  InnerContainer,
} from "components/flatlist/info-card.styles";
import { Paragraph, Label } from "components/styling/styled.components";
import {
  BackButton,
  SaveButton,
  AddToTourButton,
} from "components/buttons/modalButtons";

const { width, height } = Dimensions.get("window");

const MarkerImage = styled.Image`
  width: 48px;
  height: 48px;
`;

const OverlayComponentContainer = styled.View`
  padding-top: 8px;
  padding-left: 10px;
  padding-right: 60px;
  flex-direction: row;
`;

export const Map = () => {
  const [locations, setLocations] = useState([]);
  const [loading, setLoading] = useState(false);
  const { refresh, setRefresh } = useContext(FlatlistContext);
  const authContext = useContext(AuthContext);
  const axiosContext = useContext(AxiosContext);

  //Drop down picker states
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [dropDownData, setDropDownData] = useState([]);

  const filteredData = value
    ? locations.filter((x) =>
        x.category.toLowerCase().includes(value.toLowerCase())
      )
    : locations;

  const {
    modalVisible,
    setModalVisible,
    id,
    setId,
    title,
    setTitle,
    description,
    setDescription,
    latitude,
    setLatitude,
    longitude,
    setLongitude,
    photo,
    setPhoto,
  } = useContext(MapModalContext);

  const OverlayComponent = () => {
    return (
      <DropDownPicker
        placeholder="Select by category"
        open={open}
        value={value}
        items={dropDownData}
        setOpen={setOpen}
        setValue={setValue}
        setItems={setDropDownData}
      />
    );
  };

  useEffect(() => {
    setLoading(true);
    axiosContext.publicAxios
      .get("/locations/")
      .then((response) => {
        setLoading(false);
        let dropData = [];
        setLocations(response.data);

        response.data.forEach((element) => {
          dropData.push({
            label: element.category,
            value: element.category,
          });
        });

        dropData = dropData.filter(
          (value, index, self) =>
            index ===
            self.findIndex((t) =>
              t.label === value.label ? t.value === value.value : null
            )
        );
        dropData.push({ label: "All", value: "" });

        setDropDownData(dropData);
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert("Blunder..", "Could not retrieve locations");
      });
  }, []);

  const SaveLocation = (id) => {
    setLoading(true);
    axios
      .post("http://193.219.91.103:5100/api/user-saved-locations/", {
        location: id,
      })
      .then((response) => {
        setLoading(false);
        setRefresh(!refresh);
        Alert.alert("Success!", "Location has been saved");
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert(
          "Blunder..",
          "Perhaps location is already saved, or network error"
        );
      });
  };

  const AddToTour = (id) => {
    setLoading(true);
    axios
      .post("http://193.219.91.103:5100/api/user-saved-locations-temporary/", {
        location: id,
      })
      .then(() => {
        setLoading(false);
        setRefresh(!refresh);
        Alert.alert("Success!", "Location has been added to the custom tour");
      })
      .catch((error) => {
        setLoading(false);
        Alert.alert(
          "Blunder..",
          "Perhaps location is already added, or network error"
        );
      });
  };

  const [position, setPosition] = useState({
    latitude: 10,
    longitude: 10,
    latitudeDelta: 0.001,
    longitudeDelta: 0.001,
  });

  useEffect(() => {
    Geolocation.getCurrentPosition((pos) => {
      const crd = pos.coords;
      setPosition({
        latitude: crd.latitude,
        longitude: crd.longitude,
        latitudeDelta: 0.0421,
        longitudeDelta: 0.0421,
      });
    });
    // .catch((err) => {
    //   console.log(err);
    // });
  }, []);

  return (
    <View style={styles.container}>
      <Spinner visible={loading} />
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <ExpCardContainer>
          <InnerContainer>
            <LocationCard elevation={5}>
              <LocationCardCover key={id} source={{ uri: photo }} />
              <Info>
                <Label>{title}</Label>
                <Section>
                  <Paragraph>{description}</Paragraph>
                </Section>
                <Paragraph>{`${latitude}, ${longitude}`}</Paragraph>
              </Info>
              <View>
                {authContext?.authState?.authenticated ? (
                  <>
                    <AddToTourButton
                      onPress={() => {
                        AddToTour(id);
                        setModalVisible(false);
                      }}
                    />
                    <Divider />
                    <SaveButton
                      onPress={() => {
                        SaveLocation(id);
                        setModalVisible(false);
                      }}
                    />
                    <Divider />
                    <BackButton
                      onPress={() => {
                        setModalVisible(false);
                      }}
                    />
                  </>
                ) : (
                  <BackButton
                    onPress={() => {
                      setModalVisible(false);
                    }}
                  />
                )}
              </View>
            </LocationCard>
          </InnerContainer>
        </ExpCardContainer>
      </Modal>

      <MapView
        style={styles.map}
        mapType={"none"}
        initialRegion={{
          latitude: 54.6872,
          longitude: 25.2797,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}
        showsUserLocation={true}
        showsMyLocationButton={true}
        followsUserLocation={true}
        showsCompass={true}
        scrollEnabled={true}
        zoomEnabled={true}
        pitchEnabled={true}
      >
        {filteredData.map((val, index) => {
          return (
            <React.Fragment key={val.id}>
              <Marker
                title="You are here"
                coordinate={position}
                tracksViewChanges={false}
              >
                <MarkerImage source={require("assets/iconnew.png")} />
              </Marker>

              <Marker
                coordinate={{
                  latitude: val.address.latitude,
                  longitude: val.address.longitude,
                }}
                key={index}
                tracksViewChanges={false}
                title={val.title}
                description={val.description}
                onPress={() => {
                  setTitle(val.title);
                  setDescription(val.description);
                  setLatitude(val.address.latitude);
                  setLongitude(val.address.longitude);
                  setPhoto(val.photo);
                  setModalVisible(true);
                  setId(val.id);
                }}
              />
            </React.Fragment>
          );
        })}
        <UrlTile
          urlTemplate={"http://193.219.91.103:5204/tile/{z}/{x}/{y}.png"}
          size={256}
          zIndex={99}
          shouldReplaceMapContent={true}
        />
      </MapView>

      <OverlayComponentContainer>
        <OverlayComponent
          style={{
            position: "absolute",
          }}
        />
      </OverlayComponentContainer>
    </View>
  );
};

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    height: height,
    width: width,
    justifyContent: "flex-start",
    alignItems: "stretch",
  },
});
