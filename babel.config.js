module.exports = {
  presets: ["module:metro-react-native-babel-preset"],
  plugins: [
    [
      "module-resolver",
      {
        alias: {
          src: "./src",
          components: "./components",
          assets: "./assets",
        },
      },
    ],
  ],
};
