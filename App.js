import React, { useCallback, useContext, useEffect, useState } from "react";
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Keychain from "react-native-keychain";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Spinner from "react-native-loading-spinner-overlay";

import { ThemeProvider } from "styled-components/native";
import { theme } from "./src/infrastructure/theme/index";
import { MapModalContextProvider } from "./src/services/mapModal/mapModal.context";
import { FlatlistContextProvider } from "./src/services/flatlist/flatlist.context";
import { AuthContext } from "./src/services/authorization/AuthContext";

import { StartScreen } from "./src/features/login/screens/start.screen";
import { LoginScreen } from "./src/features/login/screens/login.screen";
import { RegisterScreen } from "./src/features/login/screens/register.screen";
import { SettingsScreen } from "./src/features/settings/screens/settings.screen";
import { ResetPasswordScreen } from "./src/features/login/screens/resetPassword.screen";
import { SavedScreen } from "./src/features/saved/screens/saved.screen";
import { ToursScreen } from "./src/features/tours/screens/tours.screen";
import { Map } from "./src/features/map/screens/map.screen";
import { LocationsIndex } from "./src/features/locations/screens/locations.index";

const Tab = createBottomTabNavigator();

const createScreenOptions = ({ route }) => {
  const iconName = TAB_ICON[route.name];
  return {
    tabBarIcon: ({ size, color }) => (
      <Ionicons name={iconName} size={size} color={color} />
    ),
  };
};

const TAB_ICON = {
  Map: "map",
  Locations: "md-location",
  Saved: "flag",
  Tours: "navigate",
  Settings: "information-circle",
};

const Stack = createStackNavigator();

const App = () => {
  const authContext = useContext(AuthContext);
  const [loading, setLoading] = useState(false);

  const loadJWT = useCallback(async () => {
    setLoading(true);
    try {
      const value = await Keychain.getGenericPassword();
      const jwt = JSON.parse(value.password);

      authContext.setAuthState({
        accessToken: jwt.accessToken || null,
        refreshToken: jwt.refreshToken || null,
        authenticated: jwt.accessToken !== null,
      });
      setLoading(false);

      // authContext.setAuthState({
      //   authenticated: true,
      // });
      // setStatus('success');
    } catch (error) {
      setLoading(false);
      //console.log(`Keychain Error: ${error.message}`);
      authContext.setAuthState({
        accessToken: null,
        refreshToken: null,
        authenticated: false,
      });
    }
  }, []);

  useEffect(() => {
    loadJWT();
  }, [loadJWT]);

  function Screens() {
    function UnauthorizedTabNavigator() {
      return (
        <Tab.Navigator screenOptions={createScreenOptions}>
          <Tab.Screen name="Map" component={Map} />
          <Tab.Screen name="Locations" component={LocationsIndex} />
          <Tab.Screen name="Tours" component={ToursScreen} />
          <Tab.Screen name="Settings" component={SettingsScreen} />
        </Tab.Navigator>
      );
    }

    function AuthorizedTabNavigator() {
      return (
        <Tab.Navigator screenOptions={createScreenOptions}>
          <Tab.Screen name="Map" component={Map} />
          <Tab.Screen name="Locations" component={LocationsIndex} />
          <Tab.Screen name="Saved" component={SavedScreen} />
          <Tab.Screen name="Tours" component={ToursScreen} />
          <Tab.Screen name="Settings" component={SettingsScreen} />
        </Tab.Navigator>
      );
    }

    return (
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName={
            authContext?.authState?.authenticated
              ? "AuthorizedScreen"
              : "StartScreen"
          }
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="StartScreen" component={StartScreen} />
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
          <Stack.Screen
            name="AuthorizedScreen"
            component={AuthorizedTabNavigator}
          />
          <Stack.Screen
            name="UnauthorizedScreen"
            component={UnauthorizedTabNavigator}
          />

          <Stack.Screen
            name="ResetPasswordScreen"
            component={ResetPasswordScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  return (
    <>
      <Spinner visible={loading} />
      <ThemeProvider theme={theme}>
        <MapModalContextProvider>
          <FlatlistContextProvider>
            <Screens />
          </FlatlistContextProvider>
        </MapModalContextProvider>
      </ThemeProvider>
    </>
  );
};
export default App;
