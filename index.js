import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import { name as appName } from "./app.json";
import { AuthContextProvider } from "./src/services/authorization/AuthContext";
import { AxiosContextProvider } from "./src/services/axios/AxiosContext";

const Root = () => {
  return (
    <AuthContextProvider>
      <AxiosContextProvider>
        <App />
      </AxiosContextProvider>
    </AuthContextProvider>
  );
};
AppRegistry.registerComponent(appName, () => Root);
