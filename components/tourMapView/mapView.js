import React, { useEffect, useState } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import MapView, { Polyline, Marker, UrlTile } from "react-native-maps";
import styled from "styled-components";
import Geolocation from "@react-native-community/geolocation";
import MapViewDirections from "react-native-maps-directions";

const MarkerImage = styled.Image`
  width: ${(props) => props.theme.sizes[3]};
  height: ${(props) => props.theme.sizes[3]};
`;

const { width, height } = Dimensions.get("window");

export const MapViewScreen = ({ route }) => {
  const coordinates = route.params.tour.locations.map((entry) => entry.address);
  console.log(route.params);

  const [position, setPosition] = useState({
    latitude: 10,
    longitude: 10,
    latitudeDelta: 0.001,
    longitudeDelta: 0.001,
  });

  useEffect(() => {
    Geolocation.getCurrentPosition((pos) => {
      const crd = pos.coords;
      setPosition({
        latitude: crd.latitude,
        longitude: crd.longitude,
        latitudeDelta: 0.0421,
        longitudeDelta: 0.0421,
      });
    });
    // .catch((err) => {
    //   console.log(err);
    // });
  }, []);

  return (
    <View style={styles.container}>
      <MapView
        mapType={"none"}
        style={styles.map}
        initialRegion={{
          latitude: route.params.tour.locations[0].address.latitude,
          longitude: route.params.tour.locations[0].address.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}
        showsUserLocation={true}
        showsMyLocationButton={true}
        followsUserLocation={true}
        showsCompass={true}
        scrollEnabled={true}
        zoomEnabled={true}
        pitchEnabled={true}
      >
        <MapViewDirections
          mode="WALKING"
          origin={coordinates[0]}
          waypoints={
            coordinates.length > 2 ? coordinates.slice(1, -1) : undefined
          }
          destination={coordinates[coordinates.length - 1]}
          optimizeWaypoints={true}
          precision="high"
          apikey={"AIzaSyCqA3gxOwaYM1ApJGBf9xCwezz0H0vaZjw"}
          strokeColor="#FF0000"
          strokeWidth={3}
          zIndex={1}
        />
        {/* Currently routing is implemented using Google API, to be changed into pgRouting solution */}
        {/* <Polyline
          coordinates={coordinates}
          strokeColor="#FF0000"
          strokeWidth={3}
          zIndex={1}
        /> */}

        {route.params.tour.locations.map((val, index) => {
          return (
            <React.Fragment key={val.id}>
              <Marker
                title="You are here"
                coordinate={position}
                tracksViewChanges={false}
              >
                <MarkerImage source={require("assets/iconnew.png")} />
              </Marker>
              <Marker
                coordinate={{
                  latitude: val.address.latitude,
                  longitude: val.address.longitude,
                }}
                key={index}
                title={val.title}
                description={val.description}
                tracksViewChanges={false}
              />
            </React.Fragment>
          );
        })}
        <UrlTile
          urlTemplate={"http://193.219.91.103:5204/tile/{z}/{x}/{y}.png"}
          zIndex={0}
        />
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    height: height,
    width: width,
    justifyContent: "flex-start",
    alignItems: "stretch",
  },
});
