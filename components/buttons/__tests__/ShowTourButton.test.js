import React from "react";
import renderer from "react-test-renderer";
import { ShowTourButton } from "components/buttons/modalButtons";

it("renders correctly across screens", () => {
  const tree = renderer.create(<ShowTourButton />).toJSON();
  expect(tree).toMatchSnapshot();
});
