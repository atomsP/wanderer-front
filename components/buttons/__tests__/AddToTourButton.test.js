import React from "react";
import renderer from "react-test-renderer";
import { AddToTourButton } from "components/buttons/modalButtons";

it("renders correctly across screens", () => {
  const tree = renderer.create(<AddToTourButton />).toJSON();
  expect(tree).toMatchSnapshot();
});
