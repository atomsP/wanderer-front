import React from "react";
import renderer from "react-test-renderer";
import { DeleteTourButton } from "components/buttons/modalButtons";

it("renders correctly across screens", () => {
  const tree = renderer.create(<DeleteTourButton />).toJSON();
  expect(tree).toMatchSnapshot();
});
