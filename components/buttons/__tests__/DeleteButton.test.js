import React from "react";
import renderer from "react-test-renderer";
import { DeleteButton } from "components/buttons/modalButtons";

it("renders correctly across screens", () => {
  const tree = renderer.create(<DeleteButton />).toJSON();
  expect(tree).toMatchSnapshot();
});
