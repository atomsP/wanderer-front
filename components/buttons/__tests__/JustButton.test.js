import React from "react";
import renderer from "react-test-renderer";
import { JustButton } from "components/buttons/modalButtons";

it("renders correctly across screens", () => {
  const tree = renderer.create(<JustButton />).toJSON();
  expect(tree).toMatchSnapshot();
});
