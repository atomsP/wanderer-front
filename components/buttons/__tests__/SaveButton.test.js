import React from "react";
import renderer from "react-test-renderer";
import { SaveButton } from "components/buttons/modalButtons";

it("renders correctly across screens", () => {
  const tree = renderer.create(<SaveButton />).toJSON();
  expect(tree).toMatchSnapshot();
});
