import React from "react";
import renderer from "react-test-renderer";
import { ShowLocationButton } from "components/buttons/modalButtons";

it("renders correctly across screens", () => {
  const tree = renderer.create(<ShowLocationButton />).toJSON();
  expect(tree).toMatchSnapshot();
});
