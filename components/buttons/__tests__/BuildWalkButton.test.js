import React from "react";
import renderer from "react-test-renderer";
import { BuildWalkButton } from "components/buttons/modalButtons";

it("renders correctly across screens", () => {
  const tree = renderer.create(<BuildWalkButton />).toJSON();
  expect(tree).toMatchSnapshot();
});
