import React from "react";
import renderer from "react-test-renderer";
import { SaveTourButton } from "components/buttons/modalButtons";

it("renders correctly across screens", () => {
  const tree = renderer.create(<SaveTourButton />).toJSON();
  expect(tree).toMatchSnapshot();
});
