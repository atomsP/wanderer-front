import React from "react";
import renderer from "react-test-renderer";
import { OutlinedButton } from "components/buttons/modalButtons";

it("renders correctly across screens", () => {
  const tree = renderer.create(<OutlinedButton />).toJSON();
  expect(tree).toMatchSnapshot();
});
