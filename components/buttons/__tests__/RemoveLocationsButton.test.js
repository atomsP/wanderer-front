import React from "react";
import renderer from "react-test-renderer";
import { RemoveLocationsButton } from "components/buttons/modalButtons";

it("renders correctly across screens", () => {
  const tree = renderer.create(<RemoveLocationsButton />).toJSON();
  expect(tree).toMatchSnapshot();
});
