import React from "react";
import renderer from "react-test-renderer";
import { SelectAsStartingPointButton } from "components/buttons/modalButtons";

it("renders correctly across screens", () => {
  const tree = renderer.create(<SelectAsStartingPointButton />).toJSON();
  expect(tree).toMatchSnapshot();
});
