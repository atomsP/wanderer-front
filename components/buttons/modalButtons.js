import React from "react";
import { Button } from "react-native-paper";
import { StyleSheet } from "react-native";

export const JustButton = ({ mode, style, ...props }) => {
  return (
    <Button
      mode="contained"
      style={[styles.button, style]}
      labelStyle={styles.text}
      mode={mode}
      {...props}
    />
  );
};

export const OutlinedButton = ({ mode, style, ...props }) => {
  return (
    <Button
      mode="contained"
      style={[styles.outlinedButton, style]}
      labelStyle={styles.text}
      mode={mode}
      {...props}
    />
  );
};

export const SelectStartingPoint = (props) => {
  return (
    <Button mode="contained" color="#457b9d" {...props}>
      Select As Tour Starting Location
    </Button>
  );
};

export const BackButton = (props) => {
  return (
    <Button mode="contained" color="#457b9d" {...props}>
      Back to the list
    </Button>
  );
};

export const SaveButton = (props) => {
  return (
    <Button mode="contained" color="#457b9d" {...props}>
      Save location
    </Button>
  );
};

export const SaveTourButton = (props) => {
  return (
    <Button mode="contained" color="#457b9d" {...props}>
      Save Tour
    </Button>
  );
};

export const ShowLocationButton = (props) => {
  return (
    <Button mode="contained" color="#457b9d" {...props}>
      Show On Map
    </Button>
  );
};

export const ShowTourButton = (props) => {
  return (
    <Button mode="contained" color="#457b9d" {...props}>
      Show Tour
    </Button>
  );
};

export const AddToTourButton = (props) => {
  return (
    <Button mode="contained" color="#457b9d" {...props}>
      Add to the new tour
    </Button>
  );
};

export const DeleteButton = (props) => {
  return (
    <Button mode="contained" color="#457b9d" {...props}>
      Delete Location
    </Button>
  );
};

export const DeleteTourButton = (props) => {
  return (
    <Button mode="contained" color="#457b9d" {...props}>
      Delete Tour
    </Button>
  );
};

export const BuildWalkButton = (props) => {
  return (
    <Button mode="contained" color="#457b9d" {...props}>
      Build Walk
    </Button>
  );
};

export const RemoveLocationsButton = () => {
  return (
    <Button mode="contained" color="#e63946">
      Remove All Locations
    </Button>
  );
};

export const BuildTourButton = (props) => {
  return (
    <Button mode="contained" color="#457b9d" {...props}>
      Build Custom Tour
    </Button>
  );
};

export const SelectAsStartingPointButton = (props) => {
  return (
    <Button mode="contained" color="#457b9d" {...props}>
      Select As Tour Starting Location
    </Button>
  );
};

const styles = StyleSheet.create({
  button: {
    width: "100%",
    marginVertical: 10,
    paddingVertical: 2,
    backgroundColor: "#457b9d",
  },
  outlinedButton: {
    width: "100%",
    marginVertical: 10,
    paddingVertical: 2,
    backgroundColor: "#1d3557",
  },
  text: {
    fontWeight: "bold",
    fontSize: 15,
    lineHeight: 26,
  },
});
