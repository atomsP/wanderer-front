import styled from "styled-components/native";
import { Card } from "react-native-paper";

export const LocationCard = styled(Card)`
  background-color: ${(props) => props.theme.colors.bg.primary};
`;

export const TourCard = styled(Card)`
  background-color: "#f5ebe0";
`;

export const LocationCardCover = styled(Card.Cover)`
  padding: ${(props) => props.theme.space[3]};
  background-color: "#f5ebe0";
`;

export const Address = styled.Text`
  font-family: ${(props) => props.theme.fonts.body};
  font-size: ${(props) => props.theme.fontSizes.caption};
  color: #262626;
`;

export const Info = styled.View`
  padding: ${(props) => props.theme.space[3]};
`;

export const Section = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ExpCardContainer = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  width: auto;
`;

export const InnerContainer = styled.View`
  width: 95%;
  height: 80%;
`;
