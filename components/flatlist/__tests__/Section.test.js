import React from "react";
import renderer from "react-test-renderer";
import { Section } from "../info-card.styles";

it("renders correctly across screens", () => {
  const tree = renderer.create(<Section />).toJSON();
  expect(tree).toMatchSnapshot();
});
