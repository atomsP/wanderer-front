import React from "react";
import renderer from "react-test-renderer";
import { InnerContainer } from "../info-card.styles";

it("renders correctly across screens", () => {
  const tree = renderer.create(<InnerContainer />).toJSON();
  expect(tree).toMatchSnapshot();
});
