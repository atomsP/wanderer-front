import React from "react";
import renderer from "react-test-renderer";
import { ExpCardContainer } from "../info-card.styles";

it("renders correctly across screens", () => {
  const tree = renderer.create(<ExpCardContainer />).toJSON();
  expect(tree).toMatchSnapshot();
});
