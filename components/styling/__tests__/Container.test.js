import React from "react";
import renderer from "react-test-renderer";
import { Container } from "../styled.components";
import { ThemeProvider } from "styled-components/native";
import { theme } from "src/infrastructure/theme/index";

it("renders correctly across screens", () => {
  const tree = renderer
    .create(
      <>
        <ThemeProvider theme={theme}>
          <Container />
        </ThemeProvider>
      </>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
