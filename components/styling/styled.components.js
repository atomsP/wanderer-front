import styled from "styled-components/native";

export const Container = styled.View`
  flex: 1;
  padding: ${(props) => props.theme.space[3]};
  background-color: ${(props) => props.theme.colors.bg.primary};
`;

export const Header = styled.Text`
  font-size: ${(props) => props.theme.fontSizes.title};
  padding: ${(props) => props.theme.space[3]};
  color: ${(props) => props.theme.colors.ui.primary};
`;

export const Label = styled.Text`
  font-size: ${(props) => props.theme.fontSizes.body};
  margin-bottom: ${(props) => props.theme.space[2]};
  margin-start: ${(props) => props.theme.space[2]};
  color: ${(props) => props.theme.colors.ui.primary};
`;

export const Paragraph = styled.Text`
  font-size: ${(props) => props.theme.fontSizes.caption};
  color: ${(props) => props.theme.colors.ui.primary};
`;

export const Link = styled.Text`
  padding-top: ${(props) => props.theme.space[3]};
  font-weight: bold;
  color: ${(props) => props.theme.colors.ui.primary};
`;

export const Row = styled.View`
  flex-direction: row;
  margin-top: ${(props) => props.theme.space[1]};
`;

export const LinkContainer = styled.TouchableOpacity`
  padding-top: ${(props) => props.theme.space[3]};
`;

export const SearchContainer = styled.View`
  padding-top: ${(props) => props.theme.space[3]};
  background-color: ${(props) => props.theme.colors.bg.primary};
`;

export const PicturePreviewContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: ${(props) => props.theme.space[3]};
`;

export const InnerPicturePreviewContainer = styled.View`
  justify-content: center;
`;

export const ButtonishComponentContainer = styled.View`
  margin-top: ${(props) => props.theme.space[2]};
  justify-conten: center;
`;
