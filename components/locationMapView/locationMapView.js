import React, { useEffect, useState } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import MapView, { Marker, UrlTile } from "react-native-maps";
import styled from "styled-components";
import Geolocation from "@react-native-community/geolocation";

const MarkerImage = styled.Image`
  width: ${(props) => props.theme.sizes[3]};
  height: ${(props) => props.theme.sizes[3]};
`;

const { width, height } = Dimensions.get("window");

export const LocationMapViewScreen = ({ route }) => {
  const { id } = route.params;

  const [position, setPosition] = useState({
    latitude: 10,
    longitude: 10,
    latitudeDelta: 0.001,
    longitudeDelta: 0.001,
  });

  useEffect(() => {
    Geolocation.getCurrentPosition((pos) => {
      const crd = pos.coords;
      setPosition({
        latitude: crd.latitude,
        longitude: crd.longitude,
        latitudeDelta: 0.0421,
        longitudeDelta: 0.0421,
      });
    });
    // .catch((err) => {
    //   console.log(err);
    // });
  }, []);

  return (
    <View style={styles.container}>
      <MapView
        mapType={"none"}
        style={styles.map}
        initialRegion={{
          latitude: route.params.location.address.latitude,
          longitude: route.params.location.address.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}
        showsUserLocation={true}
        showsMyLocationButton={true}
        followsUserLocation={true}
        showsCompass={true}
        scrollEnabled={true}
        zoomEnabled={true}
        pitchEnabled={true}
      >
        <Marker
          title="You are here"
          coordinate={position}
          tracksViewChanges={false}
        >
          <MarkerImage source={require("assets/iconnew.png")} />
        </Marker>
        <Marker
          coordinate={{
            latitude: route.params.location.address.latitude,
            longitude: route.params.location.address.longitude,
          }}
          key={id}
          tracksViewChanges={false}
          title={route.params.location.title}
          description={route.params.location.description}
        />

        <UrlTile
          urlTemplate={"http://193.219.91.103:5204/tile/{z}/{x}/{y}.png"}
          zIndex={1}
        />
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    height: height,
    width: width,
    justifyContent: "flex-start",
    alignItems: "stretch",
  },
});
