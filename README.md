# wandererFrontEndJWT

To download the latest builds look for "Download artifacts" buttons in CI/CD -> Jobs

To build and run app locally:
1. Run ***npm install --force***
2. Run ***npx react-native start*** and ***npx react-native run-android***to run on your connected device or emulator - emulator has to be already started<br>
You have to be connected to VU VPN for app to access the webserver.<br>
